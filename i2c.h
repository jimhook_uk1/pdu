/***************************************************************************************************
 * Name:    I2C Driver Header
 * File:    i2c.h
 * Author:  RDTek
 * Date:    15/06/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/06/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef I2C_H
#define I2C_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

/**************************************************************************************** Defines */
#define I2C_CLK_DIV         16 /*~400kHz with 25MHz clock)*/
#define I2C_ERR_SCL_SDA_LOW 3
#define I2C_ERR_NO_ACK      2
#define I2C_ERR_BUSY        1
#define I2C_ERR_IDLE        0
    
/******************************************************************************** Global TypeDefs */
 
/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/******************************************************************************** i2c_vTxferDone ***
 * Name:        Transfer Done
 * Parameters:  err
 * Returns:     None
 * Description: Call back to indicate that the last transfer has been completed
 **************************************************************************************************/
extern void i2c_vTxferDone(uint8_t err);

/**************************************************************************** Function Prototypes */

/******************************************************************************** i2c_u8GetStatus **
 * Name:        Get Status
 * Parameters:  None
 * Returns:     err
 * Description: Returns the status of the I2C interface
 **************************************************************************************************/
extern uint8_t i2c_u8GetStatus(void);

/************************************************************************************ i2c_u8Init ***
 * Name:        Initialise I2c Interface
 * Parameters:  None
 * Returns:     err
 * Description: Initialises the I2c interface
 **************************************************************************************************/
extern uint8_t i2c_u8Init(void);

/************************************************************************************** i2c_vRead **
 * Name:        Read
 * Parameters:  *hdr
 *              hdrSize
 *              *dest
 *              nOfB (max 255)
 * Returns:     None
 * Description: Transmit start followed by hdrSize number of bytes from hdr.
 *              Transmits restart followed first hdr then reads nOfB bytes into dest
 *              Aborts if ack is not received
 **************************************************************************************************/
extern void i2c_vRead(uint8_t *hdr, uint8_t hdrSize, uint8_t *dest, uint8_t nOfB);

/************************************************************************************* i2c_vWrite **
 * Name:        Write
 * Parameters:  *hdr
 *              hdrSize
 *              *src
 *              nOfB (max 255)
 * Returns:     None
 * Description: Transmit start followed by hdrSize number of bytes from hdr then nOfB bytes from
 *              src
 *              Aborts if ack is not received
 **************************************************************************************************/
extern void i2c_vWrite(uint8_t *hdr, uint8_t hdrSize, const uint8_t *src, uint8_t nOfB);

/************************************************************************************** i2c_vIsr ***
 * Name:        I2c Interrupt Service Routine
 * Parameters:  clockDiv
 * Returns:     err
 * Description: Initialises the I2c interface
 **************************************************************************************************/
extern void i2c_vIsr(void);

#endif /* I2C_H */
