/***************************************************************************************************
 * Name:    Hardware Interface Header
 * File:    hwif.h
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef HWIF_H
#define HWIF_H
/**************************************************************************************** Include */
#include "common.h"
#include "rms.h"

/**************************************************************************************** Defines */
#define HWIF_I2C_FAULT              0x01
#define HWIF_I2C_DISPLAY_FAULT      0x02
#define HWIF_I2C_EUI_ROM_FAULT      0x04
#define HWIF_I2C_HDC1020_FAULT      0x08
#define HWIF_I2C_RELAY_FAULT        0x10
#define HWIF_I2C_TLC59116_FAULT     0x40
#define HWIF_LED_50_DEG      0x0002
#define HWIF_LED_70_DEG      0x0004
#define HWIF_LED_CLR         0x0020
#define HWIF_LED_MASTER      0x0010
#define HWIF_LED_OK          0x0008
#define HWIF_LED_OVERRIDE    0x0001
#define HWIF_NUM_OUTPUTS     RMS_NUM_OUTPUTS
#define HWIF_TEMP_MAX        70
#define HWIF_TEMP_WARN       50

#define hwif_s16GetTemperature()    (hwif_s16Temperature)
#define hwif_u16GetHumidity()       (hwif_u16Humidity)
#define hwif_u8GetHz()              (hwif_u8Hz)

/******************************************************************************** Global TypeDefs */
enum hwif_etEvent {
    HWIF_EVT_DISPLAY_WRITE_DONE,
    HWIF_EVT_I2C_INIT_DONE,
    HWIF_EVT_NO_EVENT
};

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */
extern int16_t hwif_s16Temperature;
extern uint16_t hwif_u16Humidity;
extern uint8_t hwif_u8Hz;

/******************************************************************* Callback Function Prototypes */

/************************************************************************** hwif_vI2CDeviceEvent ***
 * Name:        I2C Device Complete Event
 * Parameters:  evt     HWIF_EVT_DISPLAY_WRITE_DONE
 *                      HWIF_EVT_I2C_INIT_DONE
 *              err     HWIF_I2C_DISPLAY_FAULT
 *                      HWIF_I2C_EUI_ROM_FAULT
 *                      HWIF_I2C_HDC1020_FAULT
 *                      HWIF_I2C_RELAY_FAULT
 *                      HWIF_I2C_TLC59116_FAULT
 * Returns:     None
 * Description: Callback to indicate that an I2C transfer sequence has completed
 **************************************************************************************************/
extern void hwif_vI2CDeviceEvent(enum hwif_etEvent evt, uint8_t err);

/************************************************************************************ hwif_vTick ***
 * Name:        Ten msec Timer Event
 * Parameters:  None
 * Returns:     None
 * Description: Ten msec timer callback
 **************************************************************************************************/
extern void hwif_vTick(void);

/**************************************************************************** Function Prototypes */

/************************************************************************************ hwif_vInit ***
 * Name:        Hardware Initialisation
 * Parameters:  None
 * Returns:     err (HWIF_I2C_FAULT)
 * Description: Initialises the microcontroller hardware
 **************************************************************************************************/
extern uint8_t hwif_u8Init(void);

/************************************************************************************ hwif_vMain ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
extern void hwif_vMain(void);

/********************************************************************************* hwif_vSetLeds ***
 * Name:        Set LED Outputs
 * Parameters:  leds
 * Returns:     None
 * Description: Sets the required led state to be written on next device transfer
 **************************************************************************************************/
extern void hwif_vSetLeds(uint16_t leds);

/******************************************************************************* hwif_vSetRelays ***
 * Name:        Set Relay Outputs
 * Parameters:  relays
 * Returns:     None
 * Description: Sets the relay outputs to the given demands
 **************************************************************************************************/
extern void hwif_vSetRelays(uint16_t relaysA, uint16_t relaysB);

#endif /* HWIF_H */
