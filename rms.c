/***************************************************************************************************
 * Name:    AC Voltage and Current RMS
 * File:    rms.c
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 * Provides the interface to the AC voltage and current sensors
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "spi.h"
#include "rms.h"

/******************************************************************************* Global Variables */
uint16_t rms_u16aAmps[RMS_NUM_OUTPUTS];
uint16_t rms_u16Volts;

/*********************************************************************** Local Macros and Defines */
/* ADC */
#define ADC_CONV_CH1_5  0x09
#define ADC_CONV_CH6_10 0x0D
#define ADC_CONV_VIN    0x11
#define ADC_MAX_I_OFF   522
#define ADC_MIN_I_OFF   502

/* Pin definitions */
#define PIN_CS1         LATEbits.LE1
#define PIN_CS2         LATEbits.LE2
#define TRIS_CS1        TRISEbits.RE1
#define TRIS_CS2        TRISEbits.RE2

/* MCP6S21 */
#define MCP6S21_CHANNEL     0x41
#define MCP6S21_GAIN        0x40
#define MCP6S21_GAIN_X1     0x00
#define MCP6S21_GAIN_X2     0x01
#define MCP6S21_GAIN_X4     0x02
#define MCP6S21_GAIN_X5     0x03
#define MCP6S21_GAIN_X8     0x04
#define MCP6S21_GAIN_X10    0x05
#define MCP6S21_GAIN_X16    0x06
#define MCP6S21_GAIN_X32    0x07

/* SPI */
#define SPI_MODE1           0xC0 /* SMP=1,CKE=1 */
#define SPI_MODE2           0x20 /* SSPEN=1,CKP=0,SPIMstr,SysClk/4 */

/* MISC */
#define LOG_SIZE            150
#define RMS_AMPS_SCALE      11313
#define RMS_VOLTS_SCALE     23232

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static bool bLogStart;
static bool bLogFinished;
static uint16_t u16aLogBuf[LOG_SIZE];

/********************************************************************** Local Function Prototypes */
static uint16_t u16CalcRms(void);
static void vSetAn2Channel(uint8_t channel);
static void vSetAn3Channel(uint8_t channel);
static void vSetAn2Gain(uint8_t gain);
static void vSetAn3Gain(uint8_t gain);
static void vSetLogChannel(uint8_t channel);

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/************************************************************************************* rms_vInit ***
 * Name:        RMS Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the microcontroller hardware for voltage and current rms measurement
 **************************************************************************************************/
void rms_vInit(void) {

    /* Analogue input */
    ADCON1 = 0x0A;  /* VREF+=VDD,VREF-=VSS,AN4-AN0 */
    //ADCON2 = 0x8A;  /* right jus,acqtime=2.56us,TAD=1.28us (@25MHz) */
    ADCON2 = 0xBA;  /* right jus,acqtime=2.56us,TAD=1.28us (@25MHz) */
    ADCON0 = ADC_CONV_CH1_5;

    /* Digital outputs */
    PIN_CS1 = 1;
    PIN_CS2 = 1;
    TRIS_CS1 = 0;
    TRIS_CS2 = 0;

    /* SPI interface */
    spi_vInit(SPI_MODE1, SPI_MODE2);

    /* Initialise PGA Devices */
    vSetAn2Gain(MCP6S21_GAIN_X1);
    vSetAn3Gain(MCP6S21_GAIN_X1);
    vSetAn2Channel(0);
    vSetAn3Channel(0);
}
 
/************************************************************************************** rms_vLog ***
 * Name:        Log
 * Parameters:  None
 * Returns:     None
 * Description: Captures a log of the ADC readings of the currently selected channel
 **************************************************************************************************/
inline void rms_vLog(void) {
    static enum etLogState {
        LOG_IDLE,
        LOG_CAPTURING
    } logState;
    static uint16_t *p;
    static uint8_t i;
    uint16_t analogIn;

    analogIn = ((uint16_t)ADRESH << 8) + ADRESL;
    if (logState == LOG_IDLE) {
        if (bLogStart) {
            p = u16aLogBuf;
            i = 0;
            bLogFinished = false;
            logState = LOG_CAPTURING;
        }
    } else {
        if (i == LOG_SIZE) {
            bLogFinished = true;
            bLogStart = false;
            logState = 0;
        } else {    
            *p++ = analogIn;
            i++;
        }
    }
}

/************************************************************************************* rms_vMain ***
 * Name:        Main RMS Task
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
void rms_vMain(void) {
    static enum etLogState2 {
        LOG_SETTING_CHANNEL,
        LOG_STARTING_LOG,
        LOG_RMS_CALC
    } logState;
    static uint8_t channel;
    uint16_t rms;

    switch (logState) {
        case LOG_SETTING_CHANNEL:
            vSetLogChannel(channel);
            logState = LOG_STARTING_LOG;
            break;
        case LOG_STARTING_LOG:
            bLogStart = true;
            logState = LOG_RMS_CALC;
            break;
        case LOG_RMS_CALC:
            if (bLogFinished) {
                rms = u16CalcRms();
                if (channel == 0) {
                    rms_u16Volts = (uint16_t) (((uint32_t) rms * RMS_VOLTS_SCALE) >> 16);
                } else {
                    rms_u16aAmps[channel - 1] =
                            (uint16_t) (((uint32_t) rms * RMS_AMPS_SCALE) >> 16);
                }
                channel++;
                if (channel == 11) {
                    channel = 0;
                }
                logState = LOG_SETTING_CHANNEL;
            }
            break;
    }
}

/******************************************************************************** Local Functions */

/************************************************************************************ u16CalcRms ***
 * Name:        Calculate RMS 
 * Parameters:  None
 * Returns:     rmsAmps
 * Description: Calculates the RMS of the contents of the Log Buffer.
 *              The contents are first passed through a moving average filter and the max and min
 *              signal values are found. The rms value is then calculated using the formula:
 *                  rms = (max - min) * SCALING
 **************************************************************************************************/
static uint16_t u16CalcRms(void) {
    uint16_t *p;
    uint16_t *q;
    uint16_t max;
    uint16_t min;
    uint16_t sum;
    uint8_t i;
    
    max = 0;
    min = 0xFFFF;
    p = u16aLogBuf;
    q = u16aLogBuf;
    sum = 0;
    for (i = 0; i < LOG_SIZE; i++) {
        if (i < 4) {
            sum += *p++;
        } else {
            if (sum > max) {
                max = sum;
            } else if (sum < min) {
                min = sum;
            }
            sum -= *q++;
            sum += *p++;
        }
    }

    return (max-min) >> 2;
}

/******************************************************************************** vSetAn2Channel ***
 * Name:        Set AN2 Channel
 * Parameters:  channel
 * Returns:     None
 * Description: Selects the current input for AN2
 **************************************************************************************************/
static void vSetAn2Channel(uint8_t channel) {
    PIN_CS1 = 0;
    (void)spi_u8TxRxData(MCP6S21_CHANNEL);
    (void)spi_u8TxRxData(channel);
    PIN_CS1 = 1;
}

/******************************************************************************** vSetAn3Channel ***
 * Name:        Set AN3 Channel
 * Parameters:  channel
 * Returns:     None
 * Description: Selects the current input for AN3
 **************************************************************************************************/
static void vSetAn3Channel(uint8_t channel) {
    PIN_CS2 = 0;
    (void)spi_u8TxRxData(MCP6S21_CHANNEL);
    (void)spi_u8TxRxData(channel);
    PIN_CS2 = 1;
}

/*********************************************************************************** vSetAn2Gain ***
 * Name:        Set AN2 Gain
 * Parameters:  gain
 * Returns:     None
 * Description: Selects the input gain for AN2
 **************************************************************************************************/
static void vSetAn2Gain(uint8_t gain) {
    PIN_CS1 = 0;
    (void)spi_u8TxRxData(MCP6S21_GAIN);
    (void)spi_u8TxRxData(gain);
    PIN_CS1 = 1;
}

/*********************************************************************************** vSetAn3Gain ***
 * Name:        Set AN3 Gain
 * Parameters:  gain
 * Returns:     None
 * Description: Selects the input gain for AN3
 **************************************************************************************************/
static void vSetAn3Gain(uint8_t gain) {
    PIN_CS2 = 0;
    (void)spi_u8TxRxData(MCP6S21_GAIN);
    (void)spi_u8TxRxData(gain);
    PIN_CS2 = 1;
}

/******************************************************************************** vSetLogChannel ***
 * Name:        Log Channel
 * Parameters:  channel (0-Vin, 1-ch1 etc)
 * Returns:     None
 * Description: Starts a log
 **************************************************************************************************/
static void vSetLogChannel(uint8_t channel) {
    switch (channel) {
        case 0: /* VIN */
            ADCON0 = ADC_CONV_VIN;
            break;
        case 1: /* Channel#1 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(0);
            break;
        case 2: /* Channel#2 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(1);
            break;
        case 3: /* Channel#3 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(2);
            break;
        case 4: /* Channel#4 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(3);
            break;
        case 5: /* Channel#5 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(4);
            break;
        case 6: /* Channel#6 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(0);
            break;
        case 7: /* Channel#7 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(1);
            break;
        case 8: /* Channel#8 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(2);
            break;
        case 9: /* Channel#9 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(3);
            break;
        case 10: /* Channel#10 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(4);
            break;
    }
}

