/***************************************************************************************************
 * Name:    MCP23017 Driver Header
 * File:    mcp23017.h
 * Author:  RDTek
 * Date:    11/02/17
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 11/02/17 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef MCP23017_H
#define MCP23017_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define MCP23017_I2C_BASE_ADD   0x40u
#define MCP23017_I2C_HDR_SIZE    2
#define MCP23017_NUM_REG        22

/* Registers */
#define MCP23017_IODIRA_REG     0x00u
#define MCP23017_IODIRB_REG     0x01u
#define MCP23017_POLA_REG       0x02u
#define MCP23017_POLB_REG       0x03u
#define MCP23017_GPINTENA_REG   0x04u
#define MCP23017_GPINTENB_REG   0x05u
#define MCP23017_DEFVALA_REG    0x06u
#define MCP23017_DEFVALB_REG    0x07u
#define MCP23017_INTCONA_REG    0x08u
#define MCP23017_INTCONB_REG    0x09u
#define MCP23017_IOCON_REG      0x0Au
#define MCP23017_GPPUA_REG      0x0Cu
#define MCP23017_GPPUB_REG      0x0Du
#define MCP23017_INTFA_REG      0x0Eu
#define MCP23017_INTFB_REG      0x0Fu
#define MCP23017_INTCAPA_REG    0x10u
#define MCP23017_INTCAPB_REG    0x11u
#define MCP23017_GPIOA_REG      0x12u
#define MCP23017_GPIOB_REG      0x13u
#define MCP23017_OLATA_REG      0x14u
#define MCP23017_OLATB_REG      0x15u

#define MCP23017_MASK_0         0x01u
#define MCP23017_MASK_1         0x02u
#define MCP23017_MASK_2         0x04u
#define MCP23017_MASK_3         0x08u
#define MCP23017_MASK_4         0x10u
#define MCP23017_MASK_5         0x20u
#define MCP23017_MASK_6         0x40u
#define MCP23017_MASK_7         0x80u

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

#endif /* LED_H */
/*
 * End of Workfile: main.c
 */

