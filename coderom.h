/***************************************************************************************************
 * Name:    CODEROM Interface Header
 * File:    coderom.h
 * Author:  RDTek
 * Date:    24/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 24/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef CODEROM_H
#define CODEROM_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/********************************************************************************* coderom_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Driver
 **************************************************************************************************/
extern void coderom_vInit(void);

/****************************************************************************** coderom_vI2cRead ***
 * Name:        I2C Read
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the CODEROM
 **************************************************************************************************/
extern void coderom_vI2cRead(uint32_t address, uint8_t *data, uint8_t nOfBytes);

/***************************************************************************** coderom_vI2cWrite ***
 * Name:        I2C Write
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the CODEROM
 **************************************************************************************************/
extern void coderom_vI2cWrite(uint32_t address, uint8_t *data, uint8_t nOfBytes);

#endif /* CODEROM_H */
