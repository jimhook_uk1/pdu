/***************************************************************************************************
 * Name:    AC Detect Header
 * File:    acdetect.h
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef ACDETECT_H
#define ACDETECT_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/****************************************************************************** acdetect_bIsCbOk ***
 * Name:        Is CB signal present
 * Parameters:  channel
 * Returns:     None
 * Description: Sets the relay outputs
 **************************************************************************************************/
extern bool acdetect_bIsCbOk(uint8_t channel);

/************************************************************************** acdetect_bIsOutputOk ***
 * Name:        Is Output signal present
 * Parameters:  channel
 * Returns:     None
 * Description: Sets the relay outputs
 **************************************************************************************************/
extern bool acdetect_bIsOutputOk(uint8_t channel);

/***************************************************************************** acdetect_u16GetHz ***
 * Name:        Get AC Frequency
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
extern uint16_t acdetect_u16GetHz(void);

/****************************************************************************** acdetect_vDetect ***
 * Name:        Detect AC
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
extern inline void acdetect_vDetect(void);

/******************************************************************************** acdetect_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises interface
 **************************************************************************************************/
extern void acdetect_vInit(void);

#endif /* ACDETECT_H */
