/***************************************************************************************************
 * Name:    Relays Interface
 * File:    relays.c
 * Author:  RDTek
 * Date:    23/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 23/08/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"
#include "mcp23017.h"
#include "relays.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* I2C */
#define I2C_ADD_MCP23017U2  MCP23017_I2C_BASE_ADD + 0x0C
#define I2C_ADD_MCP23017U30 MCP23017_I2C_BASE_ADD + 0x08
#define I2C_BUF_SIZE        2
#define I2C_HDR_SIZE        2

/********************************************************************************* Local TypeDefs */
enum relays_etTxfrId {
    RELAYS_INIT_U2,
    RELAYS_INIT_U30,
    RELAYS_WRITE_U2,
    RELAYS_WRITE_U30
};

/*********************************************************************** Local Constants (static) */
static const uint8_t u8aMcp23017U2Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0x00, /* GPB7 - GPB0 ddr = outputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};
static const uint8_t u8aMcp23017U30Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0x00, /* GPB7 - GPB0 ddr = outputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};

/*********************************************************************** Local Variables (static) */
static uint16_t u16RelaysA;
static uint16_t u16RelaysB;
static uint8_t u8aRelI2cHdr[I2C_HDR_SIZE];
static uint8_t u8aRelI2cBuf[I2C_BUF_SIZE];
static uint8_t u8State;

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/********************************************************************************** relays_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Driver
 **************************************************************************************************/
void relays_vInit(void) {
    u8State = RELAYS_INIT_U2;
}

/***************************************************************************** relays_vSetRelays ***
 * Name:        Set Relay Outputs
 * Parameters:  relays
 * Returns:     None
 * Description: Sets the relay outputs to the given demands
 **************************************************************************************************/
void relays_vSetRelays(uint16_t relaysA, uint16_t relaysB) {
    u16RelaysA = relaysA;
    u16RelaysB = relaysB;
}

/***************************************************************************** relays_u8I2cWrite ***
 * Name:        I2C Write
 * Parameters:  None
 * Returns:     lastTxfer - indicates last transfer in a sequence
 * Description: Writes data to U2 and U30
 **************************************************************************************************/
uint8_t relays_u8I2cWrite(void) {
    uint8_t lastTxfer;
    
    lastTxfer = false;
    switch (u8State) {
        case RELAYS_INIT_U2:
            u8aRelI2cHdr[0] = I2C_ADD_MCP23017U2;
            u8aRelI2cHdr[1] = MCP23017_IODIRA_REG;
            i2c_vWrite(u8aRelI2cHdr, MCP23017_I2C_HDR_SIZE, u8aMcp23017U2Init, MCP23017_NUM_REG);
            u8State = RELAYS_INIT_U30;
            break;
        case RELAYS_INIT_U30:
            u8aRelI2cHdr[0] = I2C_ADD_MCP23017U30;
            u8aRelI2cHdr[1] = MCP23017_IODIRA_REG;
            i2c_vWrite(u8aRelI2cHdr, MCP23017_I2C_HDR_SIZE, u8aMcp23017U30Init, MCP23017_NUM_REG);
            u8State = RELAYS_WRITE_U2;
            lastTxfer = true;
            break;
        case RELAYS_WRITE_U2:
            u8aRelI2cHdr[0] = I2C_ADD_MCP23017U2;
            u8aRelI2cHdr[1] = MCP23017_OLATA_REG;
            u8aRelI2cBuf[0] = (uint8_t) ((u16RelaysA >> 8) | ((u16RelaysB >> 6) & 0x0C));
            u8aRelI2cBuf[1] = 0;
            i2c_vWrite(u8aRelI2cHdr, MCP23017_I2C_HDR_SIZE, u8aRelI2cBuf, 2);
            u8State = RELAYS_WRITE_U30;
            break;
        case RELAYS_WRITE_U30:
            u8aRelI2cHdr[0] = I2C_ADD_MCP23017U30;
            u8aRelI2cHdr[1] = MCP23017_OLATA_REG;
            u8aRelI2cBuf[0] = (uint8_t) u16RelaysA;
            u8aRelI2cBuf[1] = (uint8_t) u16RelaysB;
            i2c_vWrite(u8aRelI2cHdr, MCP23017_I2C_HDR_SIZE, u8aRelI2cBuf, 2);
            u8State = RELAYS_WRITE_U2;
            lastTxfer = true;
            break;
    }
    
    return lastTxfer;
}

