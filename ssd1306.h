/***************************************************************************************************
 * Name:    SSD1306 Driver Header
 * File:    ssd1306.h
 * Author:  RDTek
 * Date:    04/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 04/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef SSD1306_H
#define SSD1306_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

/**************************************************************************************** Defines */
#define SSD1306_I2C_BASE_ADD    0x78u
#define SSD1306_I2C_HDR_SIZE    1

/* Control Byte */
#define SSD1306_CTRL_CO_CMD     0x80
#define SSD1306_CTRL_CO_DATA    0xC0
#define SSD1306_CTRL_NO_CO_CMD  0x00
#define SSD1306_CTRL_NO_CO_DATA 0x40

/* Command Bytes */
#define SSD1306_DISP_OFF            0xAE    /* (Reset) */
#define SSD1306_DISP_ON             0xAF
#define SSD1306_DISP_ON_IGN_RAM     0xA5
#define SSD1306_DISP_ON_USE_RAM     0xA4    /* (Reset) */
#define SSD1306_NOP                 0xE3
#define SSD1306_SET_CHARGE_PUMP     0x8D
#define SSD1306_SET_COLUMN_ADDRESS  0x21    /* Start,End (Reset $00, $7F */
#define SSD1306_SET_COM_HW_CONFIG   0xDA    /* $22 - Enable remap, $12 - Alt COM config (Reset $12) */
#define SSD1306_SET_COM_SCAN_NORMAL 0xC0    /* (Reset) */
#define SSD1306_SET_COM_SCAN_REVRSE 0xC8
#define SSD1306_SET_CONTRAST        0x81    /* Contrast $00 - $7F (Reset $7F) */
#define SSD1306_SET_DISP_OFFSET     0xD3    /* Offset $00 - $3F (Reset $00) */
#define SSD1306_SET_DISP_OSC        0xD5    /* (Reset $80) */
#define SSD1306_SET_DISP_STL        0x40    /* - 0x7F (Start Line $00 - $3f, Reset $00) */
#define SSD1306_SET_INVERSE         0xA7
#define SSD1306_SET_MEM_ADD_MODE    0x20    /* Mode $00 - Horiz,
                                                    $01 - Vert,
                                                    $02 - Page (Reset),
                                                    $03 Invlaid */
#define SSD1306_SET_MUX_RATIO       0xA8    /* Ratio $0F - $3F (Reset $3F) */
#define SSD1306_SET_NORMAL          0xA6    /* (Reset) */
#define SSD1306_SET_PAGE_ADDRESS    0x22    /* Start,End (Reset $00, $07 */
#define SSD1306_SET_SEG_REMAP0      0xA0    /* (Reset) */
#define SSD1306_SET_SEG_REMAP1      0xA1


/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

#endif /* SSD1306_H */
/*
 * End of Workfile: ssd1306.h
 */

