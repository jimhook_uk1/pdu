/***************************************************************************************************
 * Name:    CODEROM Interface
 * File:    coderom.c
 * Author:  RDTek
 * Date:    24/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 24/08/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "coderom.h"
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* I2C */
#define I2C_ADD      0xA2
#define I2C_HDR_SIZE 3

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static uint8_t u8aCRomI2cHdr[I2C_HDR_SIZE];

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/********************************************************************************* coderom_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Driver
 **************************************************************************************************/
void coderom_vInit(void) {
    u8aCRomI2cHdr[0] = I2C_ADD;
}

/****************************************************************************** coderom_vI2cRead ***
 * Name:        I2C Read
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the CODEROM
 **************************************************************************************************/
void coderom_vI2cRead(uint32_t address, uint8_t *data, uint8_t nOfBytes) {
    if (address >= 0x10000) {
        u8aCRomI2cHdr[0] |= 0x08;
    } else {
        u8aCRomI2cHdr[1] &= 0xF7;
    }
    u8aCRomI2cHdr[1] = (uint8_t)(address >> 8);
    u8aCRomI2cHdr[2] = (uint8_t)address;
    i2c_vRead(u8aCRomI2cHdr, I2C_HDR_SIZE, data, nOfBytes);
}

/***************************************************************************** coderom_vI2cWrite ***
 * Name:        I2C Write
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the CODEROM
 **************************************************************************************************/
void coderom_vI2cWrite(uint32_t address, uint8_t *data, uint8_t nOfBytes) {
    if (address >= 0x10000) {
        u8aCRomI2cHdr[0] |= 0x08;
    } else {
        u8aCRomI2cHdr[1] &= 0xF7;
    }
    u8aCRomI2cHdr[1] = (uint8_t)(address >> 8);
    u8aCRomI2cHdr[2] = (uint8_t)address;
    i2c_vWrite(u8aCRomI2cHdr, I2C_HDR_SIZE, data, nOfBytes);
}

