/***************************************************************************************************
 * Name:     PDU Main
 * Workfile: main.c
 * Author:   RDTek
 * Date:	 26/06/18
 *
 * Change History
 *
 *   Date    | Author | Description
 * ==========|========|=============================================================================
 *  26/06/18 | RDTek  | Original
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "ctrl.h"
#include "userif.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define vResetWatchDog()    {LATEbits.LATE1 = 1;LATEbits.LATE1 = 0;}

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */
const uint16_t appPresent @ 0x1FFF6 = 0xAAAA;

/*********************************************************************** Local Variables (static) */
static bool bI2cInitDone;
static uint8_t u8HwErr;

/********************************************************************** Local Function Prototypes */

/******************************************************************************************* main **
 * Name:        main
 * Parameters:  None
 * Returns:     None
 * Description:
 **************************************************************************************************/
void main(void) {
    static uint16_t ctr;
 
    u8HwErr = hwif_u8Init();
    ctrl_vInit();
    for(;;) {
        //vResetWatchDog();
        hwif_vMain();
        ctr++;
    }
}

/************************************************************************** hwif_vI2CDeviceEvent ***
 * Name:        I2C Device Complete Event
 * Parameters:  evt     HWIF_EVT_DISPLAY_WRITE_DONE
 *                      HWIF_EVT_I2C_INIT_DONE
 *              err     HWIF_I2C_DISPLAY_FAULT
 *                      HWIF_I2C_EUI_ROM_FAULT
 *                      HWIF_I2C_HDC1020_FAULT
 *                      HWIF_I2C_MCP23017U2_FAULT
 *                      HWIF_I2C_MCP23017U30_FAULT
 *                      HWIF_I2C_TLC59116_FAULT
 * Returns:     None
 * Description: Callback to indicate that an I2C transfer sequence has completed
 **************************************************************************************************/
void hwif_vI2CDeviceEvent(enum hwif_etEvent evt, uint8_t err) {

    switch (evt) {
        case HWIF_EVT_DISPLAY_WRITE_DONE:
            break;
        case HWIF_EVT_I2C_INIT_DONE:
            bI2cInitDone = true;
            break;
    }
    userif_vWriteDone();
}

/************************************************************************************ hwif_vTick ***
 * Name:        Ten msec Timer Event
 * Parameters:  None
 * Returns:     None
 * Description: Ten msec timer callback
 **************************************************************************************************/
void hwif_vTick(void) {
    if (bI2cInitDone) {
        userif_vMain();
        ctrl_vMain();
    }
}
