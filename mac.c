/***************************************************************************************************
 * Name:    Hardware Interface
 * File:    hwif.c
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "hdc1020.h"
#include "i2c.h"
#include "led.h"
//#include "mac_address.h"
#include "mcp23017.h"
#include "spi.h"

/******************************************************************************* Global Variables */
uint8_t hwif_u8Tick;

/*********************************************************************** Local Macros and Defines */
/* Pin definitions */
#define PIN_CS1     PORTEbits.RE1
#define PIN_CS2     PORTEbits.RE2
#define PIN_RESET   PORTEbits.RE0

/* ADC */
#define ADC_CONV_CH1_5  0x09
#define ADC_CONV_CH6_10 0x0D
#define ADC_CONV_VIN    0x11

/* CODE ROM */
#define CODEROM_I2C_HDR_SIZE 2

/* EUI ROM */
#define EUIROM_EUI_ADD      0xFA
#define EUIROM_EUI_SIZE     6
#define EUIROM_I2C_HDR_SIZE 2

/* I2C */
#define I2C_CLK_DIV     63 /*~100kHz with 25MHz clock)*/
#define I2C_ADD_CODE_ROM    0xA2
#define I2C_ADD_EUI_ROM     0xA6
#define I2C_ADD_HDC1020     HDC1020_I2C_BASE_ADD + 0x06
#define I2C_ADD_MCP23017U2  MCP23017_I2C_BASE_ADD + 0x0C
#define I2C_ADD_MCP23017U30 MCP23017_I2C_BASE_ADD + 0x08
#define I2C_ADD_TLC59116    LED_I2C_BASE_ADD

/* HDC1020 */
/* MCP6S21 */
#define MCP6S21_CHANNEL 0x41
#define MCP6S21_GAIN    0x40
#define MCP6S21_GAIN_X1 0x00
#define MCP6S21_GAIN_X2 0x01
#define MCP6S21_GAIN_X4 0x02
#define MCP6S21_GAIN_X5 0x03
#define MCP6S21_GAIN_X8 0x04
#define MCP6S21_GAIN_X10 0x05
#define MCP6S21_GAIN_X16 0x06
#define MCP6S21_GAIN_X32 0x07

/* SPI */
#define SPI_MODE1           0xC0 /* SMP=1,CKE=1 */
#define SPI_MODE2           0x20 /* SSPEN=1,CKP=0,SPIMstr,SysClk/4 */

/* TLC59116 */
#define BLINK_FREQ          23 /* 1 second */

#define CHN_1_CB_MASK   0x0001
#define CHN_1_O_MASK    0x0004
#define CHN_2_CB_MASK   0x0002
#define CHN_2_O_MASK    0x0008
#define CHN_3_CB_MASK   0x0004
#define CHN_3_O_MASK    0x0010
#define CHN_4_CB_MASK   0x0008
#define CHN_4_O_MASK    0x0020
#define CHN_5_CB_MASK   0x0010
#define CHN_5_O_MASK    0x0040
#define CHN_6_CB_MASK   0x0020
#define CHN_6_O_MASK    0x0080
#define CHN_7_CB_MASK   0x0040
#define CHN_7_O_MASK    0x0100
#define CHN_8_CB_MASK   0x0080
#define CHN_8_O_MASK    0x0200
#define CHN_9_CB_MASK   0x0100
#define CHN_9_O_MASK    0x0400
#define CHN_10_CB_MASK  0x0100
#define CHN_10_O_MASK   0x0800
#define FLT_CB          0x01
#define FLT_OPIN        0x02

#define u8GetHighByte(x)    (uint8_t)((x) >> 8)
#define u8GetLowByte(x)     (uint8_t)((x))
#define u8GetUpperByte(x)   (uint8_t)((x) >> 16)
/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */
static const uint8_t u8aLedInit[LED_NUM_REG] = {
    LED_MODE_NORMAL, /* MODE1 */
    LED_MODE_ERREN | LED_MODE_DIM | LED_MODE_STPCHG, /* MODE2 */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_MAX_BRIGHT, /* GRPPWM */
    BLINK_FREQ, /* GRPFREQ */
    LED_GRPPWM, /* LEDOUT1 */
    LED_GRPPWM, /* LEDOUT2 */
    LED_GRPPWM, /* LEDOUT3 */
    LED_GRPPWM /* LEDOUT4 */
};
static const uint8_t u8aMcp23017U2Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0x00, /* GPB7 - GPB0 ddr = outputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};
static const uint8_t u8aMcp23017U30Init[MCP23017_NUM_REG] = {
    0x00, /* GPA7 - GPA0 ddr = outputs */
    0x00, /* GPB7 - GPB0 ddr = outputs */
    0x00, /* GPA7 - GPA0 normal polarity */
    0x00, /* GPB7 - GPB0 normal polarity */
    0x00, /* GPA7 - GPA0 interrupts diabled */
    0x00, /* GPB7 - GPB0 interrupts diabled */
    0x00, /* GPA7 - GPA0 defval =0 */
    0x00, /* GPB7 - GPB0 defval =0 */
    0x00, /* GPA7 - GPA0 intcona =0 */
    0x00, /* GPB7 - GPB0 intcona =0 */
    0x08, /* IOCON */
    0x08, /* IOCON */
    0x00, /* GPA7 - GPA0 pullups disabled */
    0x00, /* GPB7 - GPB0 pullups disabled */
    0x00, /* GPA7 - GPA0 interrupt */
    0x00, /* GPB7 - GPB0 interrupt */
    0x00, /* GPA7 - GPA0 interrupt capture */
    0x00, /* GPB7 - GPB0 interrupt capture */
    0x00, /* GPA7 - GPA0 gpio */
    0x00, /* GPB7 - GPB0 gpio */
    0x00, /* GPA7 - GPA0 olat */
    0x00, /* GPB7 - GPB0 olat */
};

/*********************************************************************** Local Variables (static) */
static bool bLogStart;
static bool bLogStartSync;
static bool bLogFinished;
static uint16_t *pu16Buf;
static uint16_t u16aLogBuf[256];
static uint16_t u16Humidity;
static uint16_t u16Temperature;
static uint8_t u8aCbMask[2];
static uint8_t u8aI2cHdr[3];
static uint8_t u8aI2cBuf[128];
static uint8_t u8aOMask[2];
static uint8_t u8InFault;
static uint8_t u8LogChannel;

/********************************************************************** Local Function Prototypes */
static void vLogChannel(uint8_t channel);
static void vSetAn2Channel(uint8_t channel);
static void vSetAn3Channel(uint8_t channel);
static void vSetAn2Gain(uint8_t gain);
static void vSetAn3Gain(uint8_t gain);
static void vSetRelay(uint16_t chnA, uint16_t chnB);

/************************************************************************************ hwif_vInit ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
void hwif_vInit(void) {
    //bool euiRomOk;
    bool u2Ok;
    bool u30Ok;
    bool ledOk;
    bool hdcOk;
    uint8_t i;
    
    /* Analogue input */
    ADCON1 = 0x0A;  /* VREF+=VDD,VREF-=VSS,AN4-AN0 */
    ADCON2 = 0x8A;  /* right jus,acqtime=2.56us,TAD=1.28us (@25MHz) */
    ADCON0 = ADC_CONV_CH1_5;

    /* Digital outputs */
    LATA = 0x00;
    TRISA = 0xEF;
    LATB = 0x00;
    TRISB = 0x38;
    LATC = 0x00;
    TRISC = 0x18;
    LATD = 0x00;
    TRISD = 0x20;
    LATE = 0x07;   /* RESET=1,CS1=1,CS2=1 */
    TRISE = 0x00;   /* All ouputs */
    TRISF = 0xFF;
    LATG = 0x00;
    TRISG = 0x0F;
    TRISH = 0xFF;
    LATJ = 0x00;
    TRISJ = 0x00;

    /* I2C interface */
    i2c_vInit(I2C_CLK_DIV);
    //u8aI2cHdr[0] = I2C_ADD_EUI_ROM;
    //u8aI2cHdr[1] = EUIROM_EUI_ADD;
    //euiRomOk = i2c_bRead(u8aI2cHdr, EUIROM_I2C_HDR_SIZE, u8aI2cBuf, EUIROM_EUI_SIZE);
    //MAC_putAddress(u8aI2cBuf);
    
    u8aI2cHdr[0] = I2C_ADD_MCP23017U2;
    u8aI2cHdr[1] = MCP23017_IODIRA_REG;
    u2Ok = i2c_bWrite(u8aI2cHdr, MCP23017_I2C_HDR_SIZE, (uint8_t *)u8aMcp23017U2Init, MCP23017_NUM_REG);
    
    u8aI2cHdr[0] = I2C_ADD_MCP23017U30;
    u8aI2cHdr[1] = MCP23017_IODIRA_REG;
    u30Ok = i2c_bWrite(u8aI2cHdr, MCP23017_I2C_HDR_SIZE, (uint8_t *)u8aMcp23017U30Init, MCP23017_NUM_REG);
    
    u8aI2cHdr[0] = I2C_ADD_TLC59116;
    u8aI2cHdr[1] = LED_MODE1_REG;
    ledOk = i2c_bWrite(u8aI2cHdr, LED_I2C_HDR_SIZE, (uint8_t *)u8aLedInit, LED_NUM_REG);
    
    u8aI2cHdr[0] = I2C_ADD_HDC1020;
    u8aI2cHdr[1] = HDC1020_CONFIG_REG;
    u8aI2cBuf[0] = (HDC1020_TEMP_AND_HUMID | HDC1020_TEMP_RSLN_14 | HDC1020_HUMID_RSLN_14);
    u8aI2cBuf[1] = 0;
    hdcOk = i2c_bWrite(u8aI2cHdr, HDC1020_I2C_HDR_SIZE, u8aI2cBuf, 2);
    
    /* SPI interface */
    spi_vInit(SPI_MODE1, SPI_MODE2);

    vSetAn2Gain(MCP6S21_GAIN_X32);
    vSetAn3Gain(MCP6S21_GAIN_X1);
    vSetAn2Channel(0);
    vSetAn3Channel(0);
    
    /* Initialise Timer1 and ECCP2 to automatically perform ADC conversion every 100usecs and
     *  generate an interrupt on completion */
    TMR1H = 0;
    TMR1L = 0;
    T1CON = 0x01;
    CCPR2H = 0x02;
    CCPR2L = 0x71;
    CCP2CON = 0x0B;
    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 1;
    IPR1bits.ADIP = 1;
    RCONbits.IPEN = 1;
    INTCONbits.GIEH = 1;
}

/************************************************************************************ hwif_vMain ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
void hwif_vMain(void) {
    static uint16_t chnARelay;
    static uint16_t chnBRelay;
    static uint8_t logState;
    static uint8_t state;
    static uint8_t tickH;
    
    if (hwif_u8Tick != tickH) {
        if (hwif_u8Tick == 0) {
            switch (state) {
                case 0:
                    chnARelay = 0x0001;
                    chnBRelay = 0x0001;
                    u8aI2cHdr[0] = I2C_ADD_HDC1020;
                    u8aI2cHdr[1] = HDC1020_TEMPERATURE_REG;
                    (void)i2c_bWrite(u8aI2cHdr, HDC1020_I2C_HDR_SIZE, u8aI2cBuf, 0);
                    state = 1;
                    break;
                case 1:
                    vSetRelay(chnARelay, 0);
                    u8aI2cHdr[0] = I2C_ADD_HDC1020;
                    (void)i2c_bRead(u8aI2cHdr, 1, u8aI2cBuf, 4);
                    u16Temperature = ((uint16_t)u8aI2cBuf[0] << 8) | u8aI2cBuf[1];
                    u16Humidity = ((uint16_t)u8aI2cBuf[2] << 8) | u8aI2cBuf[3];
                    state = 2;
                    break;
                case 2:
                    vLogChannel(1);
                    state = 3;
                    break;
                case 3:
                    vSetRelay(0, 0);
                    chnARelay = chnARelay << 1;
                    chnBRelay = chnBRelay << 1;
                    if (chnARelay == 0x400) {
                        chnARelay = 0x0001;
                        chnBRelay = 0x0001;
                    }
                    //state = 4;
                    break;
            }
        }
        tickH = hwif_u8Tick;
    }
    //if (logState == 0) {
    //    vLogChannel(0);
    //    logState = 1;
    //}
}
/******************************************************************************* vHighPriorityIsr **
 * Name:        High Priority ISR
 * Parameters:  None
 * Returns:     None
 * Description: Handles the High Priority Interrupt which handles the high frequency tick.
 **************************************************************************************************/
static interrupt high_priority void vHighPriorityIsr(void){
    static bool cbPinH;
    static bool oPinH;
    static uint8_t cbTmr;
    static uint8_t cbTmrState;
    static uint8_t logIdx;
    static uint8_t logState;
    static uint8_t oPinTmr;
    static uint8_t oPinTmrState;
    static uint8_t tmr;
    bool cbPin;
    bool oPin;
    uint16_t analogIn;

    PIR1bits.ADIF = 0;
    analogIn = (uint16_t)ADRESH << 8 | ADRESL;
    
    if ((PORTH & u8aCbMask[0]) || (PORTF & u8aCbMask[1])) {
        cbPin = true;
    } else {
        cbPin = false;
    }
    if ((PORTF & u8aOMask[0]) || (PORTG & u8aOMask[1])) {
        oPin = true;
    } else {
        oPin = false;
    }

    /* cbTmr checks for existance of pulses on CB input*/
    if (cbTmrState == 0) {
        if (bLogStartSync) {
            cbPinH = cbPin;
            cbTmr = 0;
            u8InFault &= ~FLT_CB;
            cbTmrState = 1;
        }
    } else {
        if (bLogFinished) {
            cbTmrState = 0;
        } else if (cbPin != cbPinH) {
            cbTmr = 0;
            cbPinH = cbPin;
        } else if (cbTmr == 255) {
            u8InFault |= FLT_CB;
            cbTmrState = 0;
        } else {
            cbTmr++;
        }
    }

    /* oPinTmr checks for existance of pulses on the outputs
     * If a HIGH to LOW transition is detected output current logging is
     * started */
    if (oPinTmrState == 0) {
        if (bLogStartSync) {
            oPinH = oPin;
            oPinTmr = 0;
            u8InFault &= ~FLT_OPIN;
            oPinTmrState = 1;
        }
    } else {
        if (bLogFinished) {
            oPinTmrState = 0;
        } else if (oPin != oPinH) {
            oPinTmr = 0;
            if (!oPin) {
                bLogStart = true;
            }
            oPinH = oPin;
        } else if (oPinTmr == 255) {
            u8InFault |= FLT_OPIN;
            oPinTmrState = 0;
        } else {
            oPinTmr++;
        }
    }
    
    if (logState == 0) {
        if (bLogStart) {
            bLogStart = false;
            pu16Buf = u16aLogBuf;
            logIdx = 0;
            logState = 1;
        }
    } else {
        *pu16Buf++ = analogIn;
        logIdx++;
        if (logIdx == 0) { /* Overflow */
            logState = 0;
            bLogFinished = true;
        }
    }
    bLogStartSync = false;
    tmr++;
    if (tmr == 100) {
        tmr = 0;
        hwif_u8Tick++;
        if (hwif_u8Tick == 100) {
            hwif_u8Tick = 0;
        }
    }
}

/*********************************************************************************** vLogChannel ***
 * Name:        Log Channel
 * Parameters:  channel (0-Vin, 1-ch1 etc)
  * Returns:    None
 * Description: Starts a log
 **************************************************************************************************/
static void vLogChannel(uint8_t channel) {
    switch (channel) {
        case 0: /* VIN */
            ADCON0 = ADC_CONV_VIN;
            bLogStart = true;
            break;
        case 1: /* Channel#1 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(0);
            u8aCbMask[0] = u8GetLowByte(CHN_1_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_1_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_1_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_1_O_MASK);
            //bLogStartSync = true;
            bLogStart = true;
            break;
        case 2: /* Channel#2 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(1);
            u8aCbMask[0] = u8GetLowByte(CHN_2_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_2_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_2_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_2_O_MASK);
            bLogStartSync = true;
            break;
        case 3: /* Channel#3 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(2);
            u8aCbMask[0] = u8GetLowByte(CHN_3_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_3_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_3_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_3_O_MASK);
            bLogStartSync = true;
            break;
        case 4: /* Channel#4 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(3);
            u8aCbMask[0] = u8GetLowByte(CHN_4_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_4_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_4_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_4_O_MASK);
            bLogStartSync = true;
            break;
        case 5: /* Channel#5 */
            ADCON0 = ADC_CONV_CH1_5;
            vSetAn2Channel(4);
            u8aCbMask[0] = u8GetLowByte(CHN_5_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_5_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_5_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_5_O_MASK);
            bLogStartSync = true;
            break;
        case 6: /* Channel#1 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(0);
            u8aCbMask[0] = u8GetLowByte(CHN_6_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_6_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_6_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_6_O_MASK);
            bLogStartSync = true;
            break;
        case 7: /* Channel#7 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(1);
            u8aCbMask[0] = u8GetLowByte(CHN_7_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_7_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_7_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_7_O_MASK);
            bLogStartSync = true;
            break;
        case 8: /* Channel#8 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(2);
            u8aCbMask[0] = u8GetLowByte(CHN_8_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_8_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_8_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_8_O_MASK);
            bLogStartSync = true;
            break;
        case 9: /* Channel#9 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(3);
            u8aCbMask[0] = u8GetLowByte(CHN_9_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_9_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_9_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_9_O_MASK);
            bLogStartSync = true;
            break;
        case 10: /* Channel#10 */
            ADCON0 = ADC_CONV_CH6_10;
            vSetAn3Channel(4);
            u8aCbMask[0] = u8GetLowByte(CHN_10_CB_MASK);
            u8aCbMask[1] = u8GetHighByte(CHN_10_CB_MASK);
            u8aOMask[0] = u8GetLowByte(CHN_10_O_MASK);
            u8aOMask[1] = u8GetHighByte(CHN_10_O_MASK);
            bLogStartSync = true;
            break;
            
    }
}
/************************************************************************************* vSetRelay ***
 * Name:        Set Relay
 * Parameters:  chnA
 *              chnB
 * Returns:     None
 * Description: Sets the relays as indicated
 **************************************************************************************************/
static void vSetRelay(uint16_t chnA, uint16_t chnB) {
    u8aI2cHdr[0] = I2C_ADD_MCP23017U30;
    u8aI2cHdr[1] = MCP23017_OLATA_REG;
    u8aI2cBuf[0] = (uint8_t)chnA;
    u8aI2cBuf[1] = (uint8_t)chnB;
    (void)i2c_bWrite(u8aI2cHdr, MCP23017_I2C_HDR_SIZE, u8aI2cBuf, 2);
    u8aI2cHdr[0] = I2C_ADD_MCP23017U2;
    u8aI2cHdr[1] = MCP23017_OLATA_REG;
    u8aI2cBuf[0] = (uint8_t)((chnA >> 8) | ((chnB >> 6) & 0x0C));
    u8aI2cBuf[1] = 0;
    (void)i2c_bWrite(u8aI2cHdr, MCP23017_I2C_HDR_SIZE, u8aI2cBuf, 2);
}

/******************************************************************************** vSetAn2Channel ***
 * Name:        Set AN2 Channel
 * Parameters:  channel
 * Returns:     None
 * Description: Selects the current input for AN2
 **************************************************************************************************/
static void vSetAn2Channel(uint8_t channel) {
    PIN_CS1 = 0;
    (void)spi_u8TxRxData(MCP6S21_CHANNEL);
    (void)spi_u8TxRxData(channel);
    PIN_CS1 = 1;
}

/******************************************************************************** vSetAn3Channel ***
 * Name:        Set AN3 Channel
 * Parameters:  channel
 * Returns:     None
 * Description: Selects the current input for AN3
 **************************************************************************************************/
static void vSetAn3Channel(uint8_t channel) {
    PIN_CS2 = 0;
    (void)spi_u8TxRxData(MCP6S21_CHANNEL);
    (void)spi_u8TxRxData(channel);
    PIN_CS2 = 1;
}

/*********************************************************************************** vSetAn2Gain ***
 * Name:        Set AN2 Gain
 * Parameters:  gain
 * Returns:     None
 * Description: Selects the input gain for AN2
 **************************************************************************************************/
static void vSetAn2Gain(uint8_t gain) {
    PIN_CS1 = 0;
    (void)spi_u8TxRxData(MCP6S21_GAIN);
    (void)spi_u8TxRxData(gain);
    PIN_CS1 = 1;
}

/*********************************************************************************** vSetAn3Gain ***
 * Name:        Set AN3 Gain
 * Parameters:  gain
 * Returns:     None
 * Description: Selects the input gain for AN3
 **************************************************************************************************/
static void vSetAn3Gain(uint8_t gain) {
    PIN_CS2 = 0;
    (void)spi_u8TxRxData(MCP6S21_GAIN);
    (void)spi_u8TxRxData(gain);
    PIN_CS2 = 1;
}
