/***************************************************************************************************
 * Name:    Ethernet Driver Header
 * File:    hwif.h
 * Author:  RDTek
 * Date:    21/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 21/07/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef ETH_H
#define ETH_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */
#define ETH_FILTER              0b10101011; /* UCEN,OR,CRCEN,MPEN,BCEN
                                             * (unicast,crc,magic packet,multicast,broadcast) */
#define ETH_MAX_TX_PACKET_SIZE  1518
/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */
extern bool bLinkUp;
/******************************************************************* Callback Function Prototypes */

/************************************************************************************ eth_u8Init ***
 * Name:        Initialise Ethernet Driver
 * Parameters:  address
 * Returns:     err
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
extern uint8_t eth_u8Init(uint8_t *address);

/************************************************************************************* eth_vMain ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
extern void eth_vMain(void);

#endif /* ETH_H */
