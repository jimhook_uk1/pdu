/***************************************************************************************************
 * Name:    HDC1020 Driver
 * File:    hdc1020.c
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "hdc1020.h"
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define HDC1020_I2C_BASE_ADD    0x80u
#define HDC1020_I2C_HDR_SIZE    2
#define HDC1020_NUM_REG         3

/* Registers */
#define HDC1020_TEMPERATURE_REG 0x00u
#define HDC1020_HUMIDITY_REG    0x01u
#define HDC1020_CONFIG_REG      0x02u

/* Data */
#define HDC1020_SW_RST          0x80
#define HDC1020_HTR_DISABLD     0x00
#define HDC1020_HTR_ENBLD       0x20
#define HDC1020_TEMP_AND_HUMID  0x10
#define HDC1020_TEMP_RSLN_14    0x00
#define HDC1020_TEMP_RSLN_11    0x04
#define HDC1020_HUMID_RSLN_14   0x00
#define HDC1020_HUMID_RSLN_11   0x01
#define HDC1020_HUMID_RSLN_8    0x02

/* I2C */
#define I2C_ADD_HDC1020     HDC1020_I2C_BASE_ADD + 0x06
#define I2C_BUF_SIZE        4

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */
static const uint8_t u8aHdcInit[] = {
    HDC1020_TEMP_AND_HUMID | HDC1020_TEMP_RSLN_14 | HDC1020_HUMID_RSLN_14,
    0    
};

/*********************************************************************** Local Variables (static) */
static uint8_t u8aHdcI2cHdr[HDC1020_I2C_HDR_SIZE];
static uint8_t u8aHdcI2cBuf[I2C_BUF_SIZE];

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/************************************************************************* hdc1020_u8GetHumidity ***
 * Name:        Get Humidity
 * Parameters:  None
 * Returns:     humidity
 * Description: Converts read data to humidity
 **************************************************************************************************/
uint16_t hdc1020_u16GetHumidity(void) {
    return (int16_t) ((((int32_t)u8aHdcI2cBuf[2] << 8 | u8aHdcI2cBuf[3]) * 100) >> 16);
}

/********************************************************************* hdc1020_s16GetTemperature ***
 * Name:        Get Temperature
 * Parameters:  None
 * Returns:     temperature (1/1 deg C)
 * Description: Converts read data to temperature
 **************************************************************************************************/
int16_t hdc1020_s16GetTemperature(void) {
    return (int16_t)((((int32_t)u8aHdcI2cBuf[0] << 8 | u8aHdcI2cBuf[1]) * 165) >> 16) - 40;
}

/*************************************************************************** hdc1020_vGetResults ***
 * Name:        Get Results
 * Parameters:  None
 * Returns:     None
 * Description: Reads conversion results from device
 **************************************************************************************************/
void hdc1020_vGetResults(void) {
    i2c_vRead(u8aHdcI2cHdr, 1, u8aHdcI2cBuf, 4);
}

/*************************************************************************** hdc1020_vDeviceInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Writes initialisation data to the HDC1020 device
 **************************************************************************************************/
void hdc1020_vDeviceInit(void) {
    u8aHdcI2cHdr[0] = I2C_ADD_HDC1020;
    u8aHdcI2cHdr[1] = HDC1020_CONFIG_REG;
    i2c_vWrite(u8aHdcI2cHdr, HDC1020_I2C_HDR_SIZE, u8aHdcInit, sizeof(u8aHdcInit));
}

/********************************************************************** hdc1020_vStartConversion ***
 * Name:        Start Conversion
 * Parameters:  None
 * Returns:     None
 * Description: Starts a temperature and humidity conversion
 **************************************************************************************************/
void hdc1020_vStartConversion(void) {
    u8aHdcI2cHdr[1] = HDC1020_TEMPERATURE_REG;
    i2c_vWrite(u8aHdcI2cHdr, HDC1020_I2C_HDR_SIZE, u8aHdcI2cBuf, 0);
}

