/***************************************************************************************************
 * Name:    Display Header
 * File:    display.h
 * Author:  RDTek
 * Date:    04/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 04/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef DISPLAY_H
#define DISPLAY_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/********************************************************************************** display_vClr ***
 * Name:        Clear Display
 * Parameters:  None
 * Returns:     None
 * Description: Clears display
 **************************************************************************************************/
extern void display_vClr(void);

/**************************************************************************** display_u8I2cWrite ***
 * Name:        I2C Write
 * Parameters:  None
 * Returns:     lastTxfer - indicates last transfer in a sequence
 * Description: Writes data to display
 *              Upto 67 data bytes are allowed in one transfer
 **************************************************************************************************/
extern uint8_t display_u8I2cWrite(void);

/********************************************************************************* display_vInit ***
 * Name:        Display Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initiates initialisation of the display
 **************************************************************************************************/
extern void display_vInit(void);

/****************************************************************************** display_vPutChar ***
 * Name:        Send character to display
 * Parameters:  character
 * Returns:     None
 * Description: Sends a single character to the display
 **************************************************************************************************/
extern void display_vPutChar(uint8_t character);

/**************************************************************************** display_vPutString ***
 * Name:        Sends a string to display
 * Parameters:  s
 * Returns:     None
 * Description: Sends a string to display
 *              The string must be a maximum of 11 characters
 **************************************************************************************************/
extern void display_vPutString(uint8_t *s);

/******************************************************************************* display_vSetPos ***
 * Name:        Set print position
 * Parameters:  column
 *              row
 * Returns:     None
 * Description: Sets the print position for a 5*7 character
 **************************************************************************************************/
extern void display_vSetPos(uint8_t column, uint8_t row);

#endif /* DISPLAY_H */
