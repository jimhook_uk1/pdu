/***************************************************************************************************
 * Name:    TLC59116 Driver
 * File:    led.c
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"
#include "led.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* I2C */
#define I2C_ADD_TLC59116    LED_I2C_BASE_ADD
#define I2C_BUF_SIZE        4

/* I2C */
#define LED_I2C_BASE_ADD    0xC0u
#define LED_I2C_HDR_SIZE    2
#define LED_NUM_REG         24

/* Registers */
#define LED_MODE1_REG   0x00u
#define LED_MODE2_REG   0x01u
#define LED_PWM0_REG    0x02u
#define LED_PWM1_REG    0x03u
#define LED_PWM2_REG    0x04u
#define LED_PWM3_REG    0x05u
#define LED_PWM4_REG    0x06u
#define LED_PWM5_REG    0x07u
#define LED_PWM6_REG    0x08u
#define LED_PWM7_REG    0x09u
#define LED_PWM8_REG    0x0Au
#define LED_PWM9_REG    0x0Bu
#define LED_PWM10_REG   0x0Cu
#define LED_PWM11_REG   0x0Du
#define LED_PWM12_REG   0x0Eu
#define LED_PWM13_REG   0x0Fu
#define LED_PWM14_REG   0x10u
#define LED_PWM15_REG   0x11u
#define LED_GRPPWM_REG  0x12u
#define LED_GRPFREQ_REG 0x13u
#define LED_LEDOUT0_REG 0x14u
#define LED_LEDOUT1_REG 0x15u
#define LED_LEDOUT2_REG 0x16u
#define LED_LEDOUT3_REG 0x17u
#define LED_SUBADR0_REG 0x18u
#define LED_SUBADR1_REG 0x19u
#define LED_SUBADR2_REG 0x1Au
#define LED_ALLCALLADR_REG 0x1Bu
#define LED_IREF_REG    0x1Cu
#define LED_EFLAG1      0x1Du
#define LED_EFLAG2      0x1Eu

#define LED_MODE_ACKCHG 0x80u
#define LED_MODE_DIM    0x00u
#define LED_MODE_BLINK  0x20u
#define LED_MODE_ERRDIS 0x80u
#define LED_MODE_ERREN  0x00u
#define LED_MODE_STPCHG 0x00u

#define LED_GRPPWM      0x00u
#define LED_MAX_BRIGHT  0xFFu
#define LED_MODE_NORMAL 0x00u
#define LED_NO_AUTO     0x00u
#define LED_AUTO_ALL    0x80u

#define LED_AUTO_INC_1  0x80u
#define LED_AUTO_INC_2  0xA0u
#define LED_AUTO_INC_3  0xC0u
#define LED_AUTO_INC_4  0xE0u

#define LED_PWM_ON      255u
#define LED_PWM_OFF     0u

#define BLINK_FREQ      23 /* 1 second */

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */
static const uint8_t u8aLedInit[LED_NUM_REG] = {
    LED_MODE_NORMAL, /* MODE1 */
    LED_MODE_ERREN | LED_MODE_DIM | LED_MODE_STPCHG, /* MODE2 */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, LED_PWM_OFF, /* PWM */
    LED_MAX_BRIGHT, /* GRPPWM */
    BLINK_FREQ, /* GRPFREQ */
    LED_GRPPWM, /* LEDOUT1 */
    LED_GRPPWM, /* LEDOUT2 */
    LED_GRPPWM, /* LEDOUT3 */
    LED_GRPPWM /* LEDOUT4 */
};

/*********************************************************************** Local Variables (static) */
static uint16_t u16Leds;
static uint8_t u8aLedI2cHdr[LED_I2C_HDR_SIZE];
static uint8_t u8aLedI2cBuf[I2C_BUF_SIZE];

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/******************************************************************************* led_vDeviceInit ***
 * Name:        Device Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises LED Device
 **************************************************************************************************/
void led_vDeviceInit(void) {
    u8aLedI2cHdr[0] = I2C_ADD_TLC59116;
    u8aLedI2cHdr[1] = LED_MODE1_REG;
    i2c_vWrite(u8aLedI2cHdr, LED_I2C_HDR_SIZE, u8aLedInit, LED_NUM_REG);
}

/******************************************************************************** led_vWriteLeds ***
 * Name:        Write Leds
 * Parameters:  None
 * Returns:     None
 * Description: Updates LED off/on device state
 **************************************************************************************************/
void led_vWriteLeds(void) {
    uint8_t *p;
    uint8_t i;
    uint8_t leds;
    uint8_t mask;
    
    
    p = u8aLedI2cBuf;
    mask = 0x01;
    leds = u16Leds;
    for (i = 0; i< 16; i++) {
        if (leds & 0x01) {
            *p |= mask;
        } else {
            *p &= ~mask;
        }
        leds = leds >> 1;
        mask = mask << 2;
        if (mask == 0) {
            mask = 0x01;
            p++;
        }
    }
    u8aLedI2cHdr[1] = LED_AUTO_INC_1 | LED_LEDOUT0_REG;
    i2c_vWrite(u8aLedI2cHdr, LED_I2C_HDR_SIZE, u8aLedI2cBuf, 4);
}
    
/********************************************************************************** led_vSetLeds ***
 * Name:        Set LED Outputs
 * Parameters:  leds
 * Returns:     None
 * Description: Sets the required led state to be written on next device transfer
 **************************************************************************************************/
void led_vSetLeds(uint16_t leds) {
    u16Leds = leds;
}
