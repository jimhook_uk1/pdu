/***************************************************************************************************
 * Name:    HDC1020 Driver Header
 * File:    hdc1020.h
 * Author:  RDTek
 * Date:    17/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 17/07/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef HDC1020_H
#define HDC1020_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/************************************************************************* hdc1020_u8GetHumidity ***
 * Name:        Get Humidity
 * Parameters:  None
 * Returns:     humidity
 * Description: Converts read data to humidity
 **************************************************************************************************/
extern uint16_t hdc1020_u16GetHumidity(void);

/********************************************************************* hdc1020_u16GetTemperature ***
 * Name:        Get Temperature
 * Parameters:  None
 * Returns:     temperature (1/1 deg C)
 * Description: Converts read data to temperature
 **************************************************************************************************/
extern int16_t hdc1020_s16GetTemperature(void);

/*************************************************************************** hdc1020_vGetResults ***
 * Name:        Get Results
 * Parameters:  None
 * Returns:     None
 * Description: Reads conversion results from device
 **************************************************************************************************/
extern void hdc1020_vGetResults(void);

/************************************************************************** hdc1020_vDeveiceInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Writes initialisation data to the HDC1020 device
 **************************************************************************************************/
extern void hdc1020_vDeviceInit(void);

/********************************************************************** hdc1020_vStartConversion ***
 * Name:        Start Conversion
 * Parameters:  None
 * Returns:     None
 * Description: Starts a temperature and humidity conversion
 **************************************************************************************************/
extern void hdc1020_vStartConversion(void);

#endif /* HDC1020_H */

