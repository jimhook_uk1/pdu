/***************************************************************************************************
 * Name:    Control Header
 * File:    ctrl.h
 * Author:  RDTek
 * Date:    25/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 25/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef CTRL_H
#define CTRL_H
/**************************************************************************************** Include */
#include "common.h"
#include "hwif.h"

/**************************************************************************************** Defines */
#define ctrl_bGetMaster()           (ctrl_bMasterOn)
#define ctrl_bGetOverride()         (ctrl_bOverrideOn)
#define ctrl_eGetOutputState(x)     (ctrl_eaOutputStatus[(x)])
#define ctrl_u16SetUserDemand(x)    (ctrl_u16UserDemand = (x))
#define ctrl_vClrMaster()           (ctrl_bMasterOn = false)
#define ctrl_vSetMaster()           (ctrl_bMasterOn = true)
#define ctrl_vClrOverride()         (ctrl_bOverrideOn = false)
#define ctrl_vSetOverride()         (ctrl_bOverrideOn = true)

/******************************************************************************** Global TypeDefs */
enum ctrl_etOutputStatus {
    CTRL_OUTPUT_OFF,
    CTRL_OUTPUT_CB_OPEN,
    CTRL_OUTPUT_RLY_A_FLT,
    CTRL_OUTPUT_OUT_FLT,
    CTRL_OUTPUT_OUT_OK
};

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */
extern bool ctrl_bMasterOn;
extern bool ctrl_bOverrideOn;
extern enum ctrl_etOutputStatus ctrl_eaOutputStatus[HWIF_NUM_OUTPUTS];
extern uint16_t ctrl_u16UserDemand;

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/************************************************************************************ ctrl_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: 
 * 
 **************************************************************************************************/
extern void ctrl_vInit(void);

/************************************************************************************ ctrl_vMain ***
 * Name:        Main Task
 * Parameters:  None
 * Returns:     None
 * Description: Iteration Rate 10msecs
 * 
 **************************************************************************************************/
extern void ctrl_vMain(void);

#endif /* CTRL_H */
