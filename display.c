/***************************************************************************************************
 * Name:    Hardware Interface
 * File:    hwif.c
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "display.h"
#include "i2c.h"
#include "ssd1306.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define DISP_SIZE       512

/* I2C */
#define I2C_ADD_DISPLAY     SSD1306_I2C_BASE_ADD
#define I2C_BUF_SIZE        67

/********************************************************************************* Local TypeDefs */
enum etDisplayState {
    DISPLAY_CLR,
    DISPLAY_INIT,
    DISPLAY_NOP,
    DISPLAY_WRITE
};
/*********************************************************************** Local Constants (static) */
static const uint8_t u8a5x7CharSet[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, /*   */
    0x00, 0x00, 0xbe, 0x00, 0x00, /* ! */
    0x00, 0x06, 0x00, 0x06, 0x00, /* " */
    0x28, 0xfe, 0x28, 0xfe, 0x28, /* # */
    0x48, 0x54, 0xfe, 0x54, 0x24, /* $ */
    0x46, 0x26, 0x10, 0xc8, 0xc4, /* % */
    0x6c, 0x92, 0xb2, 0x4c, 0xa0, /* & */
    0x00, 0x0a, 0x06, 0x00, 0x00, /* ' */
    0x00, 0x38, 0x44, 0x82, 0x00, /* ( */
    0x00, 0x82, 0x44, 0x38, 0x00, /* ) */
    0x28, 0x10, 0x7c, 0x10, 0x28, /* * */
    0x10, 0x10, 0x7c, 0x10, 0x10, /* + */
    0x00, 0x50, 0x30, 0x00, 0x00, /* , */
    0x10, 0x10, 0x10, 0x10, 0x10, /* - */
    0x00, 0x60, 0x60, 0x00, 0x00, /* . */
    0x20, 0x10, 0x08, 0x04, 0x02, /* / */
    0x7c, 0xa2, 0x92, 0x8a, 0x7c, /* 0 */
    0x00, 0x84, 0xfe, 0x80, 0x00, /* 1 */
    0x84, 0xc2, 0xa2, 0x92, 0x8c, /* 2 */
    0x44, 0x82, 0x92, 0x92, 0x6c, /* 3 */
    0x30, 0x28, 0x24, 0xfe, 0x20, /* 4 */
    0x4e, 0x8a, 0x8a, 0x8a, 0x72, /* 5 */
    0x78, 0x94, 0x92, 0x92, 0x60, /* 6 */
    0x02, 0xe2, 0x12, 0x0a, 0x06, /* 7 */
    0x6c, 0x92, 0x92, 0x92, 0x6c, /* 8 */
    0x0c, 0x92, 0x92, 0x52, 0x3c, /* 9 */
    0x00, 0x6c, 0x6c, 0x00, 0x00, /* : */
    0x00, 0xac, 0x6c, 0x00, 0x00, /* ; */
    0x10, 0x28, 0x44, 0x82, 0x00, /* < */
    0x28, 0x28, 0x28, 0x28, 0x28, /* = */
    0x00, 0x82, 0x44, 0x28, 0x10, /* > */
    0x04, 0x02, 0xa2, 0x12, 0x0c, /* ? */
    0x64, 0x92, 0xe2, 0x82, 0x7c, /* @ */
    0xfc, 0x22, 0x22, 0x22, 0xfc, /* A */
    0xfe, 0x92, 0x92, 0x92, 0x6c, /* B */
    0x7c, 0x82, 0x82, 0x82, 0x44, /* C */
    0xfe, 0x82, 0x82, 0x44, 0x38, /* D */
    0xfe, 0x92, 0x92, 0x82, 0x82, /* E */
    0xfe, 0x12, 0x12, 0x02, 0x02, /* F */
    0x7c, 0x82, 0x82, 0xa2, 0x64, /* G */
    0xfe, 0x10, 0x10, 0x10, 0xfe, /* H */
    0x00, 0x00, 0xfe, 0x00, 0x00, /* I */
    0x42, 0x82, 0x7e, 0x02, 0x02, /* J */
    0xfe, 0x10, 0x28, 0x44, 0x82, /* K */
    0xfe, 0x80, 0x80, 0x80, 0x80, /* L */
    0xfe, 0x04, 0x18, 0x04, 0xfe, /* M */
    0xfe, 0x08, 0x10, 0x20, 0xfe, /* N */
    0x7c, 0x82, 0x82, 0x82, 0x7c, /* O */
    0xfe, 0x12, 0x12, 0x12, 0x0c, /* P */
    0x7c, 0x82, 0xa2, 0x42, 0xbc, /* Q */
    0xfe, 0x12, 0x32, 0x52, 0x8c, /* R */
    0x4c, 0x92, 0x92, 0x92, 0x64, /* S */
    0x02, 0x02, 0xfe, 0x02, 0x02, /* T */
    0x7e, 0x80, 0x80, 0x80, 0x7e, /* U */
    0x3e, 0x40, 0x80, 0x40, 0x3e, /* V */
    0x7e, 0x80, 0x7e, 0x80, 0x7e, /* W */
    0xc6, 0x28, 0x10, 0x28, 0xc6, /* X */
    0x0e, 0x10, 0xe0, 0x10, 0x0e, /* Y */
    0xc2, 0xa2, 0x92, 0x8a, 0x86, /* Z */
    0x00, 0xfe, 0x82, 0x82, 0x00, /* [ */
    0x04, 0x08, 0x10, 0x20, 0x40, /* \ */
    0x00, 0x82, 0x82, 0xfe, 0x00, /* ] */
    0x08, 0x04, 0x02, 0x04, 0x08, /* ^ */
    0x40, 0x40, 0x40, 0x40, 0x40, /* _ */
    0x00, 0x02, 0x04, 0x08, 0x00, /* ` */
    0x40, 0xa8, 0xa8, 0xa8, 0xf0, /* a */
    0xfe, 0x90, 0x90, 0x90, 0x60, /* b */
    0x70, 0x88, 0x88, 0x88, 0x00, /* c */
    0x60, 0x90, 0x90, 0x90, 0xfe, /* d */
    0x70, 0xa8, 0xa8, 0xa8, 0xb0, /* e */
    0x00, 0xfc, 0x12, 0x12, 0x04, /* f */
    0x10, 0xa8, 0xa8, 0xa8, 0x78, /* g */
    0xfe, 0x10, 0x10, 0xe0, 0x00, /* h */
    0x00, 0x00, 0xf4, 0x00, 0x00, /* i */
    0x40, 0x80, 0x74, 0x00, 0x00, /* j */
    0x00, 0xfe, 0x20, 0x50, 0x88, /* k */
    0x7e, 0x80, 0x80, 0x40, 0x00, /* l */
    0x00, 0xf0, 0x08, 0xf0, 0x08, /* m */
    0x00, 0xf8, 0x10, 0x08, 0xf0, /* n */
    0x70, 0x88, 0x88, 0x70, 0x00, /* o */
    0xf8, 0x28, 0x28, 0x10, 0x00, /* p */
    0x10, 0x28, 0x28, 0xf8, 0x80, /* q */
    0xf8, 0x10, 0x08, 0x08, 0x10, /* r */
    0x90, 0xa8, 0xa8, 0xa8, 0x48, /* s */
    0x08, 0x7e, 0x88, 0x80, 0x40, /* t */
    0x78, 0x80, 0x80, 0x80, 0x78, /* u */
    0x00, 0x38, 0x40, 0x80, 0x40, /* v */
    0x78, 0x80, 0x60, 0x80, 0x78, /* w */
    0x88, 0x50, 0x20, 0x50, 0x88, /* x */
    0x18, 0xa0, 0xa0, 0xa0, 0x78, /* y */
    0x88, 0xc8, 0xa8, 0x98, 0x88, /* z */
    0x00, 0x10, 0x6c, 0x82, 0x82, /* { */
    0x00, 0x00, 0xfe, 0x00, 0x00, /* | */
    0x00, 0x00, 0x82, 0x82, 0x6c, /* } */
    0x00, 0x10, 0x08, 0x10, 0x20, /* ~ */
    0x06, 0x06, 0x00, 0x00, 0x00  /* Degs */
};

static const uint8_t u8aDisplayClr[65] = {
    SSD1306_CTRL_NO_CO_DATA,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

static const uint8_t u8aDisplayInit[] = {
    SSD1306_CTRL_CO_CMD, SSD1306_SET_MUX_RATIO, SSD1306_CTRL_CO_CMD, 0x1F,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_DISP_OFFSET, SSD1306_CTRL_CO_CMD, 0x00,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_DISP_STL,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_SEG_REMAP1,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_COM_SCAN_REVRSE,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_COM_HW_CONFIG, SSD1306_CTRL_CO_CMD, 0x22,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_CONTRAST, SSD1306_CTRL_CO_CMD, 0x7F,
    SSD1306_CTRL_CO_CMD, SSD1306_DISP_ON_USE_RAM,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_NORMAL,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_DISP_OSC, SSD1306_CTRL_CO_CMD, 0x80,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_CHARGE_PUMP, SSD1306_CTRL_CO_CMD, 0x14,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_MEM_ADD_MODE, SSD1306_CTRL_CO_CMD, 0x00,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_PAGE_ADDRESS, SSD1306_CTRL_CO_CMD, 0x00,
    SSD1306_CTRL_CO_CMD, 0x03,
    SSD1306_CTRL_NO_CO_CMD, SSD1306_DISP_ON
};

static const uint8_t u8aGoHomePos[] = {
    SSD1306_CTRL_CO_CMD, SSD1306_SET_COLUMN_ADDRESS,
        SSD1306_CTRL_CO_CMD, 0, SSD1306_CTRL_CO_CMD, 0x7F,
    SSD1306_CTRL_CO_CMD, SSD1306_SET_PAGE_ADDRESS,
        SSD1306_CTRL_CO_CMD, 0, SSD1306_CTRL_CO_CMD, 0x03
};

static const uint8_t u8aNop[] = {
    SSD1306_CTRL_NO_CO_CMD, SSD1306_NOP
};

/*********************************************************************** Local Variables (static) */
static uint8_t *pu8DspI2cBuf;
static uint8_t u8aDspI2cHdr[SSD1306_I2C_HDR_SIZE + 1];
static uint8_t u8aDspI2cBuf[I2C_BUF_SIZE];
static uint8_t u8State;
static uint8_t u8TxfrCnt;
static uint8_t u8WrSize;

/********************************************************************** Local Function Prototypes */
static void vPutChar(uint8_t character);
static void vSetDisplayPos(uint8_t column, uint8_t row);

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/********************************************************************************** display_vClr ***
 * Name:        Clear Display
 * Parameters:  None
 * Returns:     None
 * Description: Clears display
 **************************************************************************************************/
void display_vClr(void) {
    vSetDisplayPos(0, 0);
    u8State = DISPLAY_CLR;
}

/**************************************************************************** display_u8I2cWrite ***
 * Name:        I2C Write
 * Parameters:  None
 * Returns:     lastTxfer - indicates last transfer in a sequence
 * Description: Writes data to display
 *              Upto 67 data bytes are allowed in one transfer
 **************************************************************************************************/
uint8_t display_u8I2cWrite(void) {
    uint8_t lastTxfer;
    
    lastTxfer = false;
    switch (u8State) {
        case DISPLAY_CLR:
            if (u8TxfrCnt == 0) {
                i2c_vWrite(u8aDspI2cHdr, SSD1306_I2C_HDR_SIZE, u8aGoHomePos, sizeof(u8aGoHomePos));
                u8TxfrCnt = 1;
            } else {
                i2c_vWrite(u8aDspI2cHdr, SSD1306_I2C_HDR_SIZE, u8aDisplayClr, sizeof(u8aDisplayClr));
                u8TxfrCnt++;
                if (u8TxfrCnt == 9) {
                    u8State = DISPLAY_NOP;
                    lastTxfer = true;
                }
            }
            break;
        case DISPLAY_INIT:
            i2c_vWrite(u8aDspI2cHdr, SSD1306_I2C_HDR_SIZE, u8aDisplayInit, sizeof(u8aDisplayInit));
            u8TxfrCnt = 0;
            u8State = DISPLAY_CLR;
            break;
        case DISPLAY_NOP:
            i2c_vWrite(u8aDspI2cHdr, SSD1306_I2C_HDR_SIZE, u8aNop, sizeof(u8aNop));
            lastTxfer = false;
            break;
        case DISPLAY_WRITE:
            i2c_vWrite(u8aDspI2cHdr, SSD1306_I2C_HDR_SIZE, u8aDspI2cBuf, u8WrSize);
            u8State = DISPLAY_NOP;
            lastTxfer = true;
            break;
    }
    
    return lastTxfer;
}

/********************************************************************************* display_vInit ***
 * Name:        Display Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initiates initialisation of the display
 **************************************************************************************************/
void display_vInit(void) {
    u8aDspI2cHdr[0] = I2C_ADD_DISPLAY;
    u8State = DISPLAY_INIT;    
}

/****************************************************************************** display_vPutChar ***
 * Name:        Send character to display
 * Parameters:  character
 * Returns:     None
 * Description: Sends a single character to the display
 **************************************************************************************************/
void display_vPutChar(uint8_t character) {
    pu8DspI2cBuf = u8aDspI2cBuf;
    *pu8DspI2cBuf++ = SSD1306_CTRL_NO_CO_DATA;
    vPutChar(character);
    u8WrSize = 7;
    u8State = DISPLAY_WRITE;
}

/**************************************************************************** display_vPutString ***
 * Name:        Sends a string to display
 * Parameters:  s
 * Returns:     None
 * Description: Sends a string to display
 *              The string must be a maximum of 11 characters
 **************************************************************************************************/
void display_vPutString(uint8_t *s) {
    pu8DspI2cBuf = u8aDspI2cBuf;
    *pu8DspI2cBuf++ = SSD1306_CTRL_NO_CO_DATA;
    u8WrSize = 1;
    while (*s) {
        vPutChar(*s);
        s++;
        u8WrSize += 6;
    }
    u8State = DISPLAY_WRITE;
}

/******************************************************************************* display_vSetPos ***
 * Name:        Set print position
 * Parameters:  column
 *              row
 * Returns:     None
 * Description: Sets the print position for a 5*7 character
 **************************************************************************************************/
void display_vSetPos(uint8_t column, uint8_t row) {
    vSetDisplayPos(column * 6, row);
    u8State = DISPLAY_WRITE;
}

/******************************************************************************* Global Functions */

/*************************************************************************************** putChar ***
 * Name:        Puts a character in the transmit buffer
 * Parameters:  character
 * Returns:     None
 * Description: Sets the column and row coordinates
 **************************************************************************************************/
static void vPutChar(uint8_t character) {
    uint8_t *p;
    uint16_t offset;

    offset = character - 32;
    offset = (offset << 2) + offset;
    p = (uint8_t *)u8a5x7CharSet + offset;
    *pu8DspI2cBuf++ = *p++;
    *pu8DspI2cBuf++ = *p++;
    *pu8DspI2cBuf++ = *p++;
    *pu8DspI2cBuf++ = *p++;
    *pu8DspI2cBuf++ = *p++;
    *pu8DspI2cBuf++ = 0;
}

/******************************************************************************** vSetDisplayPos ***
 * Name:        Set Display Position
 * Parameters:  column (0 - 127)
 *              row (0 - 3)
 * Returns:     None
 * Description: Sets the column and row coordinates
 **************************************************************************************************/
static void vSetDisplayPos(uint8_t column, uint8_t row) {
    u8aDspI2cBuf[0] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[1] = SSD1306_SET_COLUMN_ADDRESS;
    u8aDspI2cBuf[2] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[3] = column;
    u8aDspI2cBuf[4] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[5] = 0x7F;
    u8aDspI2cBuf[6] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[7] = SSD1306_SET_PAGE_ADDRESS;
    u8aDspI2cBuf[8] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[9] = row;
    u8aDspI2cBuf[10] = SSD1306_CTRL_CO_CMD;
    u8aDspI2cBuf[11] = 0x03;
    u8WrSize = 12;
}

