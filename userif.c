/***************************************************************************************************
 * Name:     User Interface
 * Workfile: userif.c
 * Author:   RDTek
 * Date:	 24/08/18
 *
 * Change History
 *
 *   Date    | Author | Description
 * ==========|========|=============================================================================
 *  24/08/18 | RDTek  | Original
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "ctrl.h"
#include "display.h"
#include "hwif.h"
#include "rms.h"
#include "switch.h"
#include "userif.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define BUF_SIZE            13
#define TIME_CHANNEL_CHANGE 50
#define TIME_TEMP_UPDATE    100

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */
/*
 * xx0C xx%RH xxxV xxHz
 * OUTLET:xx
 * LOAD:  xx.xA
 * STATUS:xxxxxxxxxxxxxx
 */
static const uint8_t u8aChannelPage0[] = {
    ' ', ' ', ' ', 'C', ' ', ' ', ' ', '%', 'R', 'H', 0
};
static const uint8_t u8aChannelPage1[] = "    V   Hz";
static const uint8_t u8aChannelPage2[] = "OUTLET:   ";
static const uint8_t u8aChannelPage3[] = "LOAD:        ";
static const uint8_t u8aChannelPage4[] = "STATUS:";

/* These strings must be the same length */
static const uint8_t u8aOutputOff[]      = " OFF       ";
static const uint8_t u8aOutputCbOpen[]   = " CB OPEN   ";
static const uint8_t u8aOutputRlyAFlt[]  = " RLYA FAULT";
static const uint8_t u8aOutputOutFlt[]   = " OUT FAULT ";
static const uint8_t u8aOutputOutOk[]    = " OUT OK    ";

/*********************************************************************** Local Variables (static) */
static bool bWriteDone;
static uint16_t u16Time;
static uint8_t u8aBuf[BUF_SIZE];
static uint8_t u8Channel;

/********************************************************************** Local Function Prototypes */
static void vCopyStr2Buf(const uint8_t *s);
static void vGetInt16Str(int16_t num, uint8_t *dest);
static void vGetUInt16Str1Dp(uint16_t num, uint8_t *dest);
static void vGetUInt16Str2Sf(uint16_t num, uint8_t *dest);
static void vGetUInt16Str3Sf(uint16_t num, uint8_t *dest);

/********************************************************************************* userif_vMain ***
 * Name:        Display Main
 * Parameters:  None
 * Returns:     None
 * Description: 10msecs
 **************************************************************************************************/
void userif_vMain(void) {
    static bool bClrToggled;
    static bool bClrH;
    static bool bMasterH;
    static bool bOverrideH;
    static uint16_t leds;
    static uint16_t time;
    static uint8_t channel;
    static uint8_t state;
    static uint8_t tempTime;

    /* Update temperature LEDs */
    if (tempTime == 0) {
        if (hwif_s16GetTemperature() > HWIF_TEMP_MAX) {
            leds |= HWIF_LED_70_DEG;
            leds &= ~(HWIF_LED_50_DEG | HWIF_LED_OK);
        } else if (hwif_s16GetTemperature() > HWIF_TEMP_WARN) {
            leds |= HWIF_LED_50_DEG;
            leds &= ~(HWIF_LED_70_DEG | HWIF_LED_OK);
        } else {
            leds |= HWIF_LED_OK;
            leds &= ~(HWIF_LED_70_DEG | HWIF_LED_50_DEG);
        }
        tempTime = 1;
    } else {
        tempTime++;
        if (tempTime == TIME_TEMP_UPDATE) {
            tempTime = 0;
        }
    }

    if (switch_bIsClrPressed()) {
        leds |= HWIF_LED_CLR;
        if (!bClrH) {
            bClrToggled = true;
        }
    } else {
        leds &= ~HWIF_LED_CLR;
    }
    bClrH = switch_bIsClrPressed();
    
    if (switch_bIsMasterPressed() && !bMasterH) {
        if(ctrl_bGetMaster()) {
            ctrl_vClrMaster();
            leds &= ~HWIF_LED_MASTER;
        } else {
            ctrl_vSetMaster();
            leds |= HWIF_LED_MASTER;
        }
    }
    bMasterH = switch_bIsMasterPressed();
    
    if (switch_bIsOverridePressed() && !bOverrideH) {
        if(ctrl_bGetOverride()) {
            ctrl_vClrOverride();
            leds &= ~HWIF_LED_OVERRIDE;
        } else {
            ctrl_vSetOverride();
            leds |= HWIF_LED_OVERRIDE;
        }
    }
    bOverrideH = switch_bIsOverridePressed();
    
    hwif_vSetLeds(leds);
    
    if (bWriteDone) {
        bWriteDone = false;
        switch (state) {
            case 0: /* Displaying Channel Page */
                display_vSetPos(0, 0);
                state = 1;
                break;
            case 1: 
                vCopyStr2Buf(u8aChannelPage0);
                vGetInt16Str(hwif_s16GetTemperature(), u8aBuf);
                vGetUInt16Str2Sf(hwif_u16GetHumidity(), u8aBuf + 5);
                display_vPutString(u8aBuf);
                state = 2;
                break;
            case 2:
                vCopyStr2Buf(u8aChannelPage1);
                vGetUInt16Str3Sf(rms_u16GetVolts(), u8aBuf + 1);
                vGetUInt16Str2Sf(hwif_u8GetHz(), u8aBuf + 6);
                display_vPutString(u8aBuf);
                state = 3;
                break;
            case 3:
                display_vSetPos(0, 1);
                state = 4;
                break;
            case 4:
                if (time == 0) {
                    /* Update outlet */
                    channel++;
                    if (channel == 11) {
                        channel = 1;
                    }
                    time = 1;
                } else {
                    time++;
                    if (time == TIME_CHANNEL_CHANGE) {
                        time = 0;
                    }
                }
                vCopyStr2Buf(u8aChannelPage2);
                if (channel == 10) {
                    u8aBuf[8] = '1';
                    u8aBuf[9] = '0';
                } else {
                    u8aBuf[8] = '0';
                    u8aBuf[9] = channel + '0';
                }
                display_vPutString(u8aBuf);
                state = 5;
                break;
            case 5:
                display_vSetPos(0, 2);
                state = 6;
                break;
            case 6:
                vCopyStr2Buf(u8aChannelPage3);
                vGetUInt16Str1Dp(rms_u16GetAmps(channel - 1), u8aBuf + 8);
                if (u8aBuf[11] == ' ') {
                    u8aBuf[11] = 'A';
                } else {
                    u8aBuf[12] = 'A';
                }
                display_vPutString(u8aBuf);
                state = 7;
                break;
            case 7:
                display_vSetPos(0, 3);
                state = 8;
                break;
            case 8:
                vCopyStr2Buf(u8aChannelPage4);
                display_vPutString(u8aBuf);
                state = 9;
                break;
            case 9:
                switch(ctrl_eGetOutputState(channel - 1)) {
                    case CTRL_OUTPUT_OFF:
                        vCopyStr2Buf(u8aOutputOff);
                        break;
                    case CTRL_OUTPUT_CB_OPEN:
                        vCopyStr2Buf(u8aOutputCbOpen);
                        break;
                    case CTRL_OUTPUT_RLY_A_FLT:
                        vCopyStr2Buf(u8aOutputRlyAFlt);
                        break;
                    case CTRL_OUTPUT_OUT_FLT:
                        vCopyStr2Buf(u8aOutputOutFlt);
                        break;
                    case CTRL_OUTPUT_OUT_OK:
                        vCopyStr2Buf(u8aOutputOutOk);
                        break;
                }
                display_vPutString(u8aBuf);
                state = 0;
                break;
        }
    }
}

/***************************************************************************** userif_vWriteDone ***
 * Name:        Display Write Done
 * Parameters:  None
 * Returns:     None
 * Description: Indicates that an I2C transfer to the display has completed
 **************************************************************************************************/
void userif_vWriteDone(void) {
    bWriteDone = true;
}

/********************************************************************************** vCopyStr2Buf ***
 * Name:        Copy String to Buffer
 * Parameters:  s
 * Returns:     None
 * Description: Copies string s to u8aBuf
 **************************************************************************************************/
static void vCopyStr2Buf(const uint8_t *s) {
    uint8_t *p;
    
    p = u8aBuf;
    while (*s) {
        *p++ = *s++;
    }
    *p = 0;
}    

/********************************************************************************** vGetInt16Str ***
 * Name:        Get int16_t string
 * Parameters:  num (999 to -99)
 *              dest    
 * Returns:     None
 * Description: Converts int16_t value to an ascii string with a fixed length of 3.
 *              if the number is positive or 0
 *                  The 100s char is padded with ' ' if 0
 *                  The 10s char is padded with ' ' if 0
 *              if the number is negative
 *                  The 100s char is padded with ' ' is the 10s is 0
 **************************************************************************************************/
static void vGetInt16Str(int16_t num, uint8_t *dest) {
    uint16_t div;

    if (num >= 0) {
        if (num >= 100) {
            div = num / 100;
            *dest++ = (uint8_t)div + '0';
            num = num % 100;
        } else {
            *dest++ = ' ';
        }
        if (num >= 10) {
            div = num / 10;
            *dest++ = (uint8_t)div + '0';
            num = num % 10;
        } else {
            *dest++ = ' ';
        }
        *dest++ = (uint8_t)num + '0';
    } else {
        num = -num;
        if (num >= 10) {
            *dest++ = '-';
            div = num / 10;
            *dest++ = (uint8_t)div + '0';
            num = num % 10;
        } else {
            *dest++ = ' ';
            *dest++ = '-';
        }
        *dest++ = (uint8_t)num + '0';
    }
}

/***************************************************************************** vGetUInt16Str1Dp ***
 * Name:        Get uint16_t string 1 decimal place
 * Parameters:  num (Max value 999)
 *              dest    
 * Returns:     None
 * Description: Converts uint16_t value to an ascii string with a fixed length of 4 with a decimal
 *              point between the penultimate and last digits.
 *              The 100s char is padded with 0 if 0
 *              The 10s char is padded with 0 if 0
 **************************************************************************************************/
static void vGetUInt16Str1Dp(uint16_t num, uint8_t *dest) {
    uint16_t div;

    if (num >= 100) {
        div = num / 100;
        *dest++ = (uint8_t)div + '0';
        num = num % 100;
//    } else {
//        *dest++ = '0';
    }
    if (num >= 10) {
        div = num / 10;
        *dest++ = (uint8_t)div + '0';
        num = num % 10;
    } else {
        *dest++ = '0';
    }
    *dest++ = '.';
    *dest++ = (uint8_t)num + '0';
}

/****************************************************************************** vGetUInt16Str2Sf ***
 * Name:        Get uint16_t string 2 sig figures
 * Parameters:  num (0 to 99)
 *              dest    
 * Returns:     None
 * Description: Converts uint16_t value to an ascii string with a fixed length of 2.
 *              The 10s char is padded with ' ' if 0
 **************************************************************************************************/
static void vGetUInt16Str2Sf(uint16_t num, uint8_t *dest) {
    uint16_t div;

    if (num >= 10) {
        div = num / 10;
        *dest++ = (uint8_t)div + '0';
        num = num % 10;
    } else {
        *dest++ = ' ';
    }
    *dest++ = (uint8_t)num + '0';
}

/****************************************************************************** vGetUInt16Str3Sf ***
 * Name:        Get uint16_t string 3 sig figures
 * Parameters:  num (0 to 999)
 *              dest    
 * Returns:     None
 * Description: Converts uint16_t value to an ascii string with a fixed length of 3.
 *              The 100s char is padded with ' ' if 0
 *              The 10s char is padded with ' ' if 0
 **************************************************************************************************/
static void vGetUInt16Str3Sf(uint16_t num, uint8_t *dest) {
    uint16_t div;

    if (num >= 100) {
        div = num / 100;
        *dest++ = (uint8_t)div + '0';
        num = num % 100;
    } else {
        *dest++ = ' ';
    }
    if (num >= 10) {
        div = num / 10;
        *dest++ = (uint8_t)div + '0';
        num = num % 10;
    } else {
        *dest++ = ' ';
    }
    *dest++ = (uint8_t)num + '0';
}
