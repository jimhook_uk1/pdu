/***************************************************************************************************
 * Name:    EUIROM Interface
 * File:    euirom.c
 * Author:  RDTek
 * Date:    24/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 24/08/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "euirom.h"
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
/* I2C */
#define I2C_ADD      0xA6
#define I2C_HDR_SIZE 2

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static uint8_t u8aEuiI2cHdr[I2C_HDR_SIZE];

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/********************************************************************************** euirom_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Driver
 **************************************************************************************************/
void euirom_vInit(void) {
    u8aEuiI2cHdr[0] = I2C_ADD;
}

/******************************************************************************* euirom_vI2cRead ***
 * Name:        I2C Read
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the EUIROM
 **************************************************************************************************/
void euirom_vI2cRead(uint8_t address, uint8_t *data, uint8_t nOfBytes) {
    u8aEuiI2cHdr[1] = address;
    i2c_vRead(u8aEuiI2cHdr, I2C_HDR_SIZE, data, nOfBytes);
}

/****************************************************************************** euirom_vI2cWrite ***
 * Name:        I2C Write
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the EUIROM
 **************************************************************************************************/
void euirom_vI2cWrite(uint8_t address, uint8_t *data, uint8_t nOfBytes) {
    u8aEuiI2cHdr[1] = address;
    i2c_vWrite(u8aEuiI2cHdr, I2C_HDR_SIZE, data, nOfBytes);
}

