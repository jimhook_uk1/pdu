/***************************************************************************************************
 * Name:    Ethernet Driver
 * File:    eth.c
 * Author:  RDTek
 * Date:    21/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 21/07/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 * This is the driver for the PIC18F97J60 device
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "eth.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define ETH_USE_FULL_DUPLEX

#define BUF_RAM_SIZE    8192
#define BUF_RX_START    0
#define BUF_RX_END      (BUF_TX_START - 1)
#define BUF_TX_END      (BUF_RAM_SIZE-1)
#define BUF_TX_SIZE     (((ETH_MAX_TX_PACKET_SIZE + TX_STATUS_VECTOR_SIZE + 1) >> 1) << 1) 
#define BUF_TX_START    (BUF_RAM_SIZE - BUF_TX_SIZE)
#define MIICMD_RD           0x01
#define PHY_NO_ERROR        0
#define PHY_BUSY_TIMEOUT    1
#define PHY_READ_FAIL       2
#define PHY_REG_PHCON1      0
#define PHY_REG_PHSTAT1     0x01
#define PHY_REG_PHCON2      0x10
#define PHY_REG_PHSTAT2     0x11
#define PHY_REG_PHIE        0x12
#define PHY_REG_PHIR        0x13
#define PHY_REG_PHLCON      0x14
#define PHY_TIMEOUT         90
#define TX_STATUS_VECTOR_SIZE   7

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
bool bLinkUp;
static uint16_t u16PhStat1;
static uint16_t u16PhStat2;
uint16_t u16ErrataTemp @ 0xE7E;

/********************************************************************** Local Function Prototypes */
//#define vWriteMac(reg, data) {(reg) = (data); NOP()}
inline uint8_t u8ReadEdata(void);
inline uint8_t u8WaitForPhyRdy(void);
inline void vWriteEdata(uint8_t d);
static uint8_t u8ReadPhy(uint8_t reg, uint16_t *data);
static uint8_t u8WritePhy(uint8_t reg, uint16_t data);
static void vGetLinkState(void);

/************************************************************************************ eth_u8Init ***
 * Name:        Initialise Ethernet Driver
 * Parameters:  address
 * Returns:     err
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
uint8_t eth_u8Init(uint8_t *address) {
    uint8_t err;

    /* Enable Ethernet */
    ECON2 = 0xA0; /* AUTOINC, ETHEN */
    
    /* Wait for PHYRDY (can take 1ms) */
    while(!ESTATbits.PHYRDY) {
    }

    MACON1 = 0x01;      /* Half duplex value (no flow control) */
    NOP();
    MACON3 = 0xB2;      /* Byte stuffing for short packets, Normal frames and headers, 
                         * half duplex */
    NOP();
    MACON4 = 0x40;      /* Half Duplex - Wait forever for access to the wire */
    NOP();
    MABBIPG = 0x12;     /* Correct gap for half duplex */
    NOP();
    MAIPG = 0x0C12;     /* Correct gap for half duplex */
    NOP();
    EFLOCON = 0x00;     /* Half duplex flow control off */

    /* Set up TX/RX buffer addresses */
    ETXST = BUF_TX_START; 
    ETXND = BUF_TX_END; 
    ERXST = BUF_RX_START;
    ERXND = BUF_RX_END;

    // Setup EDATA Pointers
    ERDPT = BUF_RX_START;
    EWRPT = BUF_TX_START;

    // Setup RXRDRDPT to a dummy value for the first packet
    ERXRDPT = BUF_RX_END;
    
    MAMXFL  = ETH_MAX_TX_PACKET_SIZE;

    /* Setup MAC address to MADDR registers */
    MAADR1 = address[0];
    NOP();
    MAADR2 = address[1];
    NOP();
    MAADR3 = address[2];
    NOP();
    MAADR4 = address[3];
    NOP();
    MAADR5 = address[4];
    NOP();
    MAADR6 = address[5];
    NOP();

    /* Configure the receive filter */
    ERXFCON = ETH_FILTER; //UCEN,OR,CRCEN,MPEN,BCEN (unicast,crc,magic packet,multicast,broadcast)

    // RXEN enabled
    ECON1=0x04;  

    /* Configure Phy registers */
    err = u8WritePhy(PHY_REG_PHCON1, 0x0000);   /* PHY is half duplex */
    if (err == PHY_NO_ERROR) {
        err = u8WritePhy(PHY_REG_PHCON2, 0x0110); /* Do not transmit loopback */
    }
    if (err == PHY_NO_ERROR) {
        err = u8WritePhy(PHY_REG_PHLCON, 0x1D4A); /* LED control - LEDA = TX/RX, LEDB = Link,
                                                   * Stretched LED */
    }
    if (err == PHY_NO_ERROR) {
        err = u8WritePhy(PHY_REG_PHIE,0x0012);
    }
    vGetLinkState();
    
    return err;
}

/************************************************************************************* eth_vMain ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
void eth_vMain(void){
    uint16_t data;
    
   if (EIRbits.LINKIF) {
       EIRbits.LINKIF = 0;
        (void)u8ReadPhy(PHY_REG_PHIR, &data); // clear the link irq
        vGetLinkState();
    }
}

/*********************************************************************************** u8ReadEdata ***
 * Name:        Read EDATA
 * Parameters:  None
 * Returns:     EDATA
 * Description: Reads EDATA
 **************************************************************************************************/
inline uint8_t u8ReadEdata(void) {
    asm("movff EDATA,_u16ErrataTemp");
    return (uint8_t) u16ErrataTemp;
}

/************************************************************************************* u8ReadPhy ***
 * Name:        Read from PHY
 * Parameters:  reg
 *              data
 * Returns:     err
 * Description: Reads from PHY register
 **************************************************************************************************/
static uint8_t u8ReadPhy(uint8_t reg, uint16_t *data) {
    int8_t err;
    
    err = PHY_NO_ERROR;
    MIREGADR = reg;
    NOP();
    MICMDbits.MIIRD = 1;
    if (u8WaitForPhyRdy() == PHY_BUSY_TIMEOUT) {
        err = PHY_READ_FAIL;
    } else {
        MICMDbits.MIIRD = 0;
        *data = MIRD;
    }
    return err;
}

/******************************************************************************* u8WaitForPhyRdy ***
 * Name:        Wait for PHY ready
 * Parameters:  None
 * Returns:     err
 * Description: Waits for PHY operation to complete
 **************************************************************************************************/
inline uint8_t u8WaitForPhyRdy(void) {
    uint8_t err;
    uint8_t timeout;

    err = PHY_NO_ERROR;
    for(timeout = 0; timeout < 10; timeout++) {
        NOP();
    }
    timeout = PHY_TIMEOUT;
    while (MISTATbits.BUSY && --timeout) {
        NOP();
    } 

    if (timeout == 0) {
        err = PHY_BUSY_TIMEOUT;
    }
    return err;
}

/************************************************************************************ u8WritePhy ***
 * Name:        Write to PHY
 * Parameters:  reg
 *              data
 * Returns:     err
 * Description: Writes to PHY register and waits for operation to complete
 **************************************************************************************************/
static uint8_t u8WritePhy(uint8_t reg, uint16_t data) {
    uint8_t gieSave;

    MIREGADR = reg;
    u16ErrataTemp = data;
    gieSave = INTCON;
    INTCON = 0;
    MIWR = u16ErrataTemp;
    INTCON = gieSave;				// Restore GIEH and GIEL value
    return u8WaitForPhyRdy();
}

/********************************************************************************* vGetLinkState ***
 * Name:        Get Link State
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
static void vGetLinkState(void) {
    uint16_t data;
    
    if (u8ReadPhy(PHY_REG_PHSTAT1, &data) == PHY_NO_ERROR) {
        u16PhStat1 = data;
    }
    if (u8ReadPhy(PHY_REG_PHSTAT2, &data) == PHY_NO_ERROR) {
        u16PhStat2 = data;
        if (data != 0) {
            bLinkUp = true;
        } else {
            bLinkUp = false;
        }
    } else {
        bLinkUp = false;
    }
}

/*********************************************************************************** vWriteEdata ***
 * Name:        Write EDATA
 * Parameters:  d
 * Returns:     None
 * Description: Write d to EDATA
 **************************************************************************************************/
inline void vWriteEdata(uint8_t d) {
    asm("movff WREG,EDATA");
}




