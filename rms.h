/***************************************************************************************************
 * Name:    AC Voltage and Current RMS Header
 * File:    rms.h
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef RMS_H
#define RMS_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */
#define RMS_NUM_OUTPUTS     10

#define rms_u16GetAmps(x)   rms_u16aAmps[(x)]
#define rms_u16GetVolts()   rms_u16Volts

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */
extern uint16_t rms_u16aAmps[RMS_NUM_OUTPUTS];
extern uint16_t rms_u16Volts;

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/************************************************************************************* rms_vInit ***
 * Name:        RMS Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the microcontroller hardware for voltage and current rms measurement
 **************************************************************************************************/
extern void rms_vInit(void);

/************************************************************************************** rms_vLog ***
 * Name:        Log
 * Parameters:  None
 * Returns:     None
 * Description: Captures a log of the ADC readings of the currently selected channel
 **************************************************************************************************/
extern inline void rms_vLog(void);

/************************************************************************************* rms_vMain ***
 * Name:        Main RMS Task
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
extern void rms_vMain(void);

#endif /* RMS_H */
