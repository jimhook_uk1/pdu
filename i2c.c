/***************************************************************************************************
 * Name:    I2C Driver
 * File:    i2c.c
 * Author:  RDTek
 * Date:    15/06/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 15/06/18 | RDTek | Created.
 *
 * Description:
 * I2C Master
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "i2c.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
//#define LOG
//#define LOG_CONT
#define LOG_ONCE
#define LOG_SIZE 200

#define ENABLE_I2C      0x28   /* Enables I2C on MSSP in Master Mode */
#define PIN_SCL         PORTCbits.RC3
#define PIN_SDA         PORTCbits.RC4
#define READ            0x01

/********************************************************************************* Local TypeDefs */
enum eI2cState {
    STATE_IDLE,
    STATE_READ,
    STATE_START_RD,
    STATE_START_WR,
    STATE_STOP,
    STATE_TX_ACK,
    STATE_WRITE,
    STATE_WRITE_RH 
};

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static bool bRead;
static enum eI2cState eState;
static uint8_t *pu8WrData;
static uint8_t *pu8Hdr;
static uint8_t *pu8Hdr0;
static uint8_t *pu8RdData;
static uint8_t u8DataSize;
static uint8_t u8Err;
static uint8_t u8HdrSize;

#ifdef LOG
static uint8_t tstLog[LOG_SIZE];
static uint8_t tstIdx;
static uint8_t *pTst;
#endif
/********************************************************************** Local Function Prototypes */

/******************************************************************************** i2c_u8GetStatus **
 * Name:        Get Status
 * Parameters:  None
 * Returns:     err
 * Description: Returns the status of the I2C interface
 **************************************************************************************************/
uint8_t i2c_u8GetStatus(void) {
    return u8Err;
}

/************************************************************************************** i2c_vRead **
 * Name:        Read
 * Parameters:  *hdr
 *              hdrSize
 *              *dest
 *              nOfB (max 255)
 * Returns:     None
 * Description: Initiates an I2C Read transaction
 **************************************************************************************************/
void i2c_vRead(uint8_t *hdr, uint8_t hdrSize, uint8_t *dest, uint8_t nOfB) {
    u8Err = I2C_ERR_BUSY;
    pu8Hdr = hdr;
    pu8Hdr0 = hdr;
    u8HdrSize = hdrSize;
    pu8RdData = dest;
    u8DataSize = nOfB;
    bRead = true;
    if (hdrSize == 1) {
        eState = STATE_START_RD;
    } else {
        eState = STATE_START_WR;
    }
    SSP1CON2bits.SEN = 1;
}

/************************************************************************************ i2c_vWrite **
 * Name:        Write
 * Parameters:  *hdr
 *              hdrSize
 *              *src
 *              nOfB (max 255)
 * Returns:     None
 * Description: Initiates an I2C write transaction
 **************************************************************************************************/
void i2c_vWrite(uint8_t *hdr, uint8_t hdrSize, const uint8_t *src, uint8_t nOfB) {
        u8Err = I2C_ERR_BUSY;
        pu8Hdr = hdr;
        u8HdrSize = hdrSize;
        pu8WrData = (uint8_t *)src;
        u8DataSize = nOfB;
        bRead = false;
        eState = STATE_START_WR;
        SSP1CON2bits.SEN = 1;
}

/************************************************************************************ i2c_u8Init ***
 * Name:        Initialise I2c Interface
 * Parameters:  None
 * Returns:     err
 * Description: Initialises the I2c interface
 **************************************************************************************************/
uint8_t i2c_u8Init(void) {
    
    if ((PIN_SCL == 0) || (PIN_SDA == 0)) {
        u8Err = I2C_ERR_SCL_SDA_LOW;
    } else {
        SSP1ADD = I2C_CLK_DIV;
        SSP1CON1 = ENABLE_I2C;
        PIR1bits.SSP1IF = 0;
        PIE1bits.SSP1IE = 1;
        IPR1bits.SSP1IP = 1;
        u8Err = I2C_ERR_IDLE;
    }

#ifdef LOG
    pTst = tstLog;
#endif
    return u8Err;
}

/************************************************************************************** i2c_vIsr ***
 * Name:        I2c Interrupt Service Routine
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
void i2c_vIsr(void) {
    static uint8_t err;
    uint8_t data;
    
    if (PIR1bits.SSP1IF == 1) {
        PIR1bits.SSP1IF = 0;
        
        switch (eState) {
            case STATE_READ:
                SSP1CON2bits.RCEN = 0;
                data = SSP1BUF;
                *pu8RdData++ = data;
                u8DataSize--;
#ifdef LOG
                *pTst++ = data;
                tstIdx++;
#endif
                if (u8DataSize) {
                    SSP1CON2bits.ACKDT1 = 0;
                } else {
                    SSP1CON2bits.ACKDT1 = 1;
                }
                SSP1CON2bits.ACKEN1 = 1;
                eState = STATE_TX_ACK;
                break;
            case STATE_START_RD:
                /* Transmit Read Address Byte */
                data = *pu8Hdr0 | READ;
                SSP1BUF = data;
                eState = STATE_WRITE_RH;
#ifdef LOG
                *pTst++ = data;
                tstIdx++;
#endif                    
                break;
            case STATE_START_WR:
                /* Transmit Write Address Byte */
                data = *pu8Hdr++;
#ifdef LOG
                *pTst++ = data;
                tstIdx++;
#endif
                SSP1BUF = data;
                u8HdrSize--;
                eState = STATE_WRITE;
                break;
            case STATE_STOP:
                u8Err = I2C_ERR_IDLE; /* This line needs to be before callback so transfers
                                       * can be chained */
                eState = STATE_IDLE;
                i2c_vTxferDone(err);
                break;
            case STATE_TX_ACK:
                /* Data left to read */
                if (u8DataSize) {
                    SSP1CON2bits.RCEN = 1;
                    eState = STATE_READ;
                    
                /* Finished reading */
                } else {
                    SSP1CON2bits.PEN = 1;
                    err = I2C_ERR_IDLE;
                    eState = STATE_STOP;
                }
                break;
            case STATE_WRITE:
                /* No acknowledge */
                if (SSP1CON2bits.ACKSTAT) {
                    SSP1CON2bits.PEN = 1;
                    err = I2C_ERR_NO_ACK;
                    eState = STATE_STOP;

                /* Header bytes left */
                } else if (u8HdrSize) {
                    data = *pu8Hdr++;
                    SSP1BUF = data;
                    u8HdrSize--;
                    /* eState = STATE_WRITE; */
#ifdef LOG
                    *pTst++ = data;
                    tstIdx++;
#endif

                /* Doing a read - restart */
                } else if (bRead) {
                    SSP1CON2bits.RSEN = 1;
                    eState = STATE_START_RD;

                /* Writing data */
                } else if (u8DataSize) {
                    data = *pu8WrData++;
                    SSP1BUF = data;
                    u8DataSize--;
                    /* eState = STATE_WRITE; */
#ifdef LOG
                    *pTst++ = data;
                    tstIdx++;
#endif

                /* Finished writing */
                } else {
                    SSP1CON2bits.PEN = 1;
                    err = I2C_ERR_IDLE;
                    eState = STATE_STOP;
                }
                break;
             case STATE_WRITE_RH:
                /* No acknowledge */
                if (SSP1CON2bits.ACKSTAT) {
                    SSP1CON2bits.PEN = 1;
                    err = I2C_ERR_NO_ACK;
                    eState = STATE_STOP;

                } else { /* Start Read */
                    SSP1CON2bits.RCEN = 1;
                    eState = STATE_READ;
                }
                break;
       }
#ifdef LOG
        if (tstIdx == LOG_SIZE) {
#ifdef LOG_CONT
            tstIdx = 0;
            pTst = tstLog;
#else
            tstIdx--;
            pTst--;
#endif
        }
#endif
    }
}

