/***************************************************************************************************
 * Name:    SPI Driver Header
 * File:    spi.h
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef SPI_H
#define SPI_H
/**************************************************************************************** Include */
#include <xc.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/************************************************************************************* spi_vInit ***
 * Name:        Initialise SPI Interface
 * Parameters:  mode1 (Written to SSPSTAT register)
 *              mode2 (Written to SSPCON register)
 * Returns:     None
 * Description: Initialises the SPI interface
 **************************************************************************************************/
extern void spi_vInit(uint8_t mode1, uint8_t mode2);

/******************************************************************************** spi_u8TxRxData ***
 * Name:        Transmit/Receive Data
 * Parameters:  txData
 * Returns:     rxData
 * Description: Transfers data on the SPI bus
 **************************************************************************************************/
extern uint8_t spi_u8TxRxData(uint8_t txData);

#endif /* SPI_H */
