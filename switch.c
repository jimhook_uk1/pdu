/***************************************************************************************************
 * Name:    Switch Interface
 * File:    switch.c
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "eth.h"
#include "hdc1020.h"
#include "hwif.h"
#include "i2c.h"
#include "led.h"
//#include "mac_address.h"
#include "mcp23017.h"
#include "spi.h"
#include "ssd1306.h"

/******************************************************************************* Global Variables */
bool switch_bClr;
bool switch_bMaster;
bool switch_bOverride;

/*********************************************************************** Local Macros and Defines */
/* Pin definitions */
#define PIN_CLR         PORTBbits.RB1
#define PIN_MASTER      PORTBbits.RB2
#define PIN_OVERRIDE    PORTBbits.RB0
#define TRIS_CLR        TRISBbits.RB1
#define TRIS_MASTER     TRISBbits.RB2
#define TRIS_OVERRIDE   TRISBbits.RB0

#define SW_CONFIRM_TIME     5

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */

/********************************************************************** Local Function Prototypes */
static void vDebounceClr(void);
static void vDebounceMaster(void);
static void vDebounceOverride(void);

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/********************************************************************************** switch_vInit ***
 * Name:        Switch Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the switch interface
 **************************************************************************************************/
void switch_vInit(void) {
    TRIS_CLR = 1;
    TRIS_OVERRIDE = 1;
    TRIS_MASTER = 1;
}

/********************************************************************************** switch_vMain ***
 * Name:        Switch Main Task
 * Parameters:  None
 * Returns:     None
 * Description: Iteration Rate - 10msecs
 **************************************************************************************************/
void switch_vMain(void) {
    vDebounceClr();
    vDebounceMaster();
    vDebounceOverride();
}
 
/******************************************************************************** Local Functions */

/********************************************************************************** vDebounceClr ***
 * Name:        Debounce CLR
 * Parameters:  None
 * Returns:     None
 * Description: Debounces CLR switch input
 **************************************************************************************************/
static void vDebounceClr(void) {
    static uint8_t clrCnt;
    static uint8_t state;

    switch (state) {
        case 1: /* Not pressed */
            if (PIN_CLR == 0) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bClr = true;
                    state = 2;
                }
            } else {
                clrCnt = 0;
            }
            break;
        case 2: /* pressed */
            if (PIN_CLR == 1) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bClr = false;
                    state = 1;
                }
            }
            break;
        default: /* Init */
            if (PIN_CLR == 0) {
                state = 2;
                switch_bClr = true;
            } else {
                switch_bClr = false;
                state = 1;
            }
            break;
            
    }
}

/******************************************************************************* vDebounceMaster ***
 * Name:        Debounce MASTER
 * Parameters:  None
 * Returns:     None
 * Description: Debounces MASTER switch input
 **************************************************************************************************/
static void vDebounceMaster(void) {
    static uint8_t clrCnt;
    static uint8_t state;

    switch (state) {
        case 1: /* Not pressed */
            if (PIN_MASTER == 0) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bMaster = true;
                    state = 2;
                }
            } else {
                clrCnt = 0;
            }
            break;
        case 2: /* pressed */
            if (PIN_MASTER == 1) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bMaster = false;
                    state = 1;
                }
            }
            break;
        default: /* Init */
            if (PIN_MASTER == 0) {
                state = 2;
                switch_bMaster = true;
            } else {
                switch_bMaster = false;
                state = 1;
            }
            break;
            
    }
}

/***************************************************************************** vDebounceOverride ***
 * Name:        Debounce OVERRIDE
 * Parameters:  None
 * Returns:     None
 * Description: Debounces OVERRIDE switch input
 **************************************************************************************************/
static void vDebounceOverride(void) {
    static uint8_t clrCnt;
    static uint8_t state;

    switch (state) {
        case 1: /* Not pressed */
            if (PIN_OVERRIDE == 0) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bOverride = true;
                    state = 2;
                }
            } else {
                clrCnt = 0;
            }
            break;
        case 2: /* pressed */
            if (PIN_OVERRIDE == 1) {
                clrCnt++;
                if (clrCnt == SW_CONFIRM_TIME) {
                    clrCnt = 0;
                    switch_bOverride = false;
                    state = 1;
                }
            }
            break;
        default: /* Init */
            if (PIN_OVERRIDE == 0) {
                state = 2;
                switch_bOverride = true;
            } else {
                switch_bOverride = false;
                state = 1;
            }
            break;
            
    }
}
