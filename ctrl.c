/***************************************************************************************************
 * Name:     Control
 * Workfile: ctrl.c
 * Author:   RDTek
 * Date:	 24/08/18
 *
 * Change History
 *
 *   Date    | Author | Description
 * ==========|========|=============================================================================
 *  24/08/18 | RDTek  | Original
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "acdetect.h"
#include "ctrl.h"

/******************************************************************************* Global Variables */
bool ctrl_bMasterOn;
bool ctrl_bOverrideOn;
enum ctrl_etOutputStatus ctrl_eaOutputStatus[HWIF_NUM_OUTPUTS];
uint16_t ctrl_u16UserDemand;

/*********************************************************************** Local Macros and Defines */
#define ALL_OUTPUTS         0x3FF
#define LOAD_SHED_INTERVAL  6000 /* 5mins */
#define OUTPUT_OFF_INTERVAL 10 /* 500msecs */
#define OUTPUT_ON_INTERVAL  10 /* 500msecs */

/********************************************************************************* Local TypeDefs */
enum etRelayState {
    RLY_OFF,
    RLY_A_ON,
    RLY_B_ON
};

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static bool bLoadShed;
static enum etRelayState eaRelayState[HWIF_NUM_OUTPUTS];
static uint16_t u16OutDemand;
static uint16_t u16OutStatus;

/********************************************************************** Local Function Prototypes */
static void vDemand(void);
static void vSetRelays(void);

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/************************************************************************************ ctrl_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: 
 * 
 **************************************************************************************************/
void ctrl_vInit(void) {
    ctrl_u16UserDemand = ALL_OUTPUTS; 
}

/************************************************************************************ ctrl_vMain ***
 * Name:        Main Task
 * Parameters:  None
 * Returns:     None
 * Description: Iteration Rate 10msecs
 * 
 **************************************************************************************************/
void ctrl_vMain(void) {
    static uint8_t time;
    
    switch (time) {
        case 0:
            vDemand();
            time = 1;
            break;
        case 1:
            vSetRelays();
            time = 2;
            break;
        case 2:
            time = 3;
            break;
        case 3:
            time = 4;
            break;
        default:
            time = 0;
            break;
    }
}

/*************************************************************************************** vDemand ***
 * Name:        Demand
 * Parameters:  None
 * Returns:     None
 * Description: Iteration Rate: 50msecs
 **************************************************************************************************/
static void vDemand(void) {
    static bool hot;
    static uint16_t interval;
    static uint8_t state;
    uint16_t offDemand;
    uint16_t offMask;
    uint16_t onDemand;
    uint16_t onMask;
    
    if (hwif_s16GetTemperature() > HWIF_TEMP_MAX) {
        hot = true;
    } else if (hwif_s16GetTemperature() < HWIF_TEMP_WARN) {
        hot = false;
    }
    if (ctrl_bMasterOn && !ctrl_bOverrideOn && hot) {
        bLoadShed = true;
    } else {
        bLoadShed = false;
    }

    switch (state) {
        case 0: /* Idle */
            if (bLoadShed || !ctrl_bMasterOn) {
                offDemand = ALL_OUTPUTS;
                onDemand = 0;
            } else {
                offDemand = ~ctrl_u16UserDemand;
                onDemand = ctrl_u16UserDemand;
            }
            offMask = 0x200;
            onMask = 0x001;
            while ((offMask != 0) && ((offDemand & offMask & u16OutStatus) == 0) &&
                    ((onDemand & onMask & ~u16OutStatus) == 0)) {
                offMask = offMask >> 1;
                onMask = onMask << 1;
            }
            if (offMask) {
                if ((offDemand & offMask & u16OutStatus) != 0) {
                    u16OutDemand &= ~offMask;
                    if (bLoadShed) {
                        state = 1;
                    } else {
                        state = 2;
                    }
                } else {
                    u16OutDemand |= onMask;
                    state = 3;
                }
                interval = 0;
            }
            break;
        case 1: /* Load shedding/switching off */
            if (bLoadShed && (interval == LOAD_SHED_INTERVAL)) {
                state = 0;
            } else if (!bLoadShed && (interval >= OUTPUT_OFF_INTERVAL)) {
                state = 0;
            } else {
                interval++;
            }
            break;
        case 2: /* Switching Off */
            if (interval == OUTPUT_OFF_INTERVAL) {
                state = 0;
            } else {
                interval++;
            }
            break;
        case 3: /* Switching On */
            if (interval == OUTPUT_ON_INTERVAL) {
                state = 0;
            } else {
                interval++;
            }
            break;
            
    }
}

/************************************************************************************ vSetRelays ***
 * Name:        Set Relay Outputs
 * Parameters:  None
 * Returns:     None
 * Description: Sets the relay outputs
 **************************************************************************************************/
static void vSetRelays(void) {
    uint16_t chnARelay;
    uint16_t chnBRelay;
    uint16_t rMask;
    uint8_t i;
    
    chnARelay = 0;
    chnBRelay = 0;
    rMask = 0x0001;
    for (i = 0; i < HWIF_NUM_OUTPUTS; i++) {
        switch (eaRelayState[i]) {
            case RLY_OFF:
                if (acdetect_bIsCbOk(i)) {
                    if (u16OutDemand & rMask) {
                        eaRelayState[i] = RLY_A_ON;
                        chnARelay |= rMask;
                        chnBRelay &= ~rMask;
                    } else {
                        chnARelay &= ~rMask;
                        chnBRelay &= ~rMask;
                    }
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_OFF;
                } else {
                    chnARelay &= ~rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_CB_OPEN;
                }
                break;
                
            case RLY_A_ON:
                if (!acdetect_bIsCbOk(i)) {
                    eaRelayState[i] = RLY_OFF;
                    chnARelay &= ~rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_CB_OPEN;
                } else if ((u16OutDemand & rMask) == 0) {
                    eaRelayState[i] = RLY_OFF;
                    chnARelay &= ~rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_OFF;
                } else if (!acdetect_bIsOutputOk(i)) {
                    chnARelay |= rMask;
                    chnBRelay |= rMask;
                    eaRelayState[i] = RLY_B_ON;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_RLY_A_FLT;
                } else {
                    chnARelay |= rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_OUT_OK;
                }
                break;
               
            case RLY_B_ON:
                if (!acdetect_bIsCbOk(i)) {
                    eaRelayState[i] = RLY_OFF;
                    chnARelay &= ~rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_CB_OPEN;
                } else if ((u16OutDemand & rMask) == 0) {
                    eaRelayState[i] = RLY_OFF;
                    chnARelay &= ~rMask;
                    chnBRelay &= ~rMask;
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_OFF;
                } else if (!acdetect_bIsOutputOk(i)) {
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_OUT_FLT;
                    chnARelay &= ~rMask;
                    chnBRelay |= rMask;
                 } else {
                    ctrl_eaOutputStatus[i] = CTRL_OUTPUT_RLY_A_FLT;
                    chnARelay &= ~rMask;
                    chnBRelay |= rMask;
                 }
               break;
        }
        rMask = rMask << 1;
    }
    u16OutStatus = chnARelay | chnBRelay;
    hwif_vSetRelays(chnARelay, chnBRelay);
}

