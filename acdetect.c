/***************************************************************************************************
 * Name:    AC Detect
 * File:    acdetect.c
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "acdetect.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */
#define TIME_25MSECS    125

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static uint8_t u8aSignalOk[3];
static uint8_t u8Hz;

/********************************************************************** Local Function Prototypes */

/***************************************************************************** Callback Functions */

/******************************************************************************* Global Functions */

/****************************************************************************** acdetect_bIsCbOk ***
 * Name:        Is CB signal present
 * Parameters:  channel
 * Returns:     None
 * Description: Sets the relay outputs
 **************************************************************************************************/
bool acdetect_bIsCbOk(uint8_t channel) {
    bool rVal;
    uint8_t mask;
    
    rVal = false;
    if (channel < 8) {
        mask = 0x01 << channel;
        if (u8aSignalOk[0] & mask) {
            rVal = true;
        }
    } else {
        mask = 0x01 << (channel - 8);
        if (u8aSignalOk[1] & mask) {
            rVal = true;
        }
    }
    return rVal;
}

/************************************************************************** acdetect_bIsOutputOk ***
 * Name:        Is Output signal present
 * Parameters:  channel
 * Returns:     None
 * Description: Sets the relay outputs
 **************************************************************************************************/
bool acdetect_bIsOutputOk(uint8_t channel) {
    bool rVal;
    uint8_t mask;
    
    rVal = false;
    if (channel < 6) {
        mask = 0x04 << channel;
        if (u8aSignalOk[1] & mask) {
            rVal = true;
        }
    } else {
        mask = 0x01 << (channel - 6);
        if (u8aSignalOk[2] & mask) {
            rVal = true;
        }
    }
    return rVal;
}

/***************************************************************************** acdetect_u16GetHz ***
 * Name:        Get AC Frequency
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
uint16_t acdetect_u16GetHz(void) {
    return 5000 / u8Hz;
}

/****************************************************************************** acdetect_vDetect ***
 * Name:        Detect AC
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
inline void acdetect_vDetect(void) {
    static uint8_t aOneDetect[3];
    static uint8_t aZeroDetect[3];
    static uint8_t hzTmr;
    static uint8_t tmr;
    uint8_t port[3];
    static uint8_t portHist;

    port[0] = PORTH;
    port[1] = PORTF;
    port[2] = PORTG;
    
    /* Voltage detection on CB and O inputs */
    if (tmr == 0) {
        u8aSignalOk[0] = aOneDetect[0] & ~aZeroDetect[0];
        u8aSignalOk[1] = aOneDetect[1] & ~aZeroDetect[1];
        u8aSignalOk[2] = aOneDetect[2] & ~aZeroDetect[2];
        aOneDetect[0] = port[0];
        aOneDetect[1] = port[1];
        aOneDetect[2] = port[2];
        aZeroDetect[0] = port[0];
        aZeroDetect[1] = port[1];
        aZeroDetect[2] = port[2];
        tmr++;
    } else {
        aOneDetect[0] |= port[0];
        aOneDetect[1] |= port[1];
        aOneDetect[2] |= port[2];
        aZeroDetect[0] &= port[0];
        aZeroDetect[1] &= port[1];
        aZeroDetect[2] &= port[2];
        tmr++;
        if (tmr == TIME_25MSECS) {
            tmr = 0;
        }
    }
        
    /* Frequency */
    if (((port[0] & 0x01) == 0) && ((portHist & 0x01) == 1) ){
        u8Hz = hzTmr;
        hzTmr = 0; 
    } else {
        hzTmr++;
    }
    portHist = port[0];
}

/******************************************************************************** acdetect_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises interface
 **************************************************************************************************/
void acdetect_vInit(void) {
    TRISF = 0xFF;
    TRISG |= 0x0F;
    TRISH = 0xFF;
}

