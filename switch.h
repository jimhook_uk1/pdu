/***************************************************************************************************
 * Name:    Switch Interface Header
 * File:    switch.h
 * Author:  RDTek
 * Date:    18/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 18/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef SWITCH_H
#define SWITCH_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */
#define switch_bIsClrPressed()        (switch_bClr)
#define switch_bIsOverridePressed()   (switch_bOverride)
#define switch_bIsMasterPressed()     (switch_bMaster)

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */
extern bool switch_bClr;
extern bool switch_bMaster;
extern bool switch_bOverride;

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/********************************************************************************** switch_vInit ***
 * Name:        Switch Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the switch interface
 **************************************************************************************************/
extern void switch_vInit(void);

/********************************************************************************** switch_vMain ***
 * Name:        Switch Main Task
 * Parameters:  None
 * Returns:     None
 * Description: Iteration Rate - 10msecs
 **************************************************************************************************/
extern void switch_vMain(void);

#endif /* SWITCH_H */
