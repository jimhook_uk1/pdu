/***************************************************************************************************
 * Name:    SPI Driver
 * File:    spi.c
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "spi.h"

/******************************************************************************* Global Variables */

/*********************************************************************** Local Macros and Defines */

/********************************************************************************* Local TypeDefs */

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */

/********************************************************************** Local Function Prototypes */

/************************************************************************************* spi_vInit ***
 * Name:        Initialise SPI Interface
 * Parameters:  mode1 (Written to SSPSTAT register)
 *              mode2 (Written to SSPCON register)
 * Returns:     None
 * Description: Initialises the SPI interface
 **************************************************************************************************/
void spi_vInit(uint8_t mode1, uint8_t mode2) {
    SSP2STAT = mode1;
    SSP2CON1 = mode2;
}

/******************************************************************************** spi_u8TxRxData ***
 * Name:        Transmit/Receive Data
 * Parameters:  txData
 * Returns:     rxData
 * Description: Transfers data on the SPI bus
 **************************************************************************************************/
uint8_t spi_u8TxRxData(uint8_t txData) {
     
    SSP2BUF = txData;
    while (SSP2STATbits.BF == 0) {
    }
    return SSP2BUF;
}
