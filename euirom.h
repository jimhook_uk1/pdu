/***************************************************************************************************
 * Name:    EUIROM Interface Header
 * File:    euiroms.h
 * Author:  RDTek
 * Date:    24/08/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 24/08/18 | RDTek | Created.
 *
 * Description:
 **************************************************************************************************/

#ifndef EUIROM_H
#define EUIROM_H
/**************************************************************************************** Include */
#include "common.h"

/**************************************************************************************** Defines */

/******************************************************************************** Global TypeDefs */

/******************************************************************************* Global Constants */

/******************************************************************************* Global Variables */

/******************************************************************* Callback Function Prototypes */

/**************************************************************************** Function Prototypes */

/********************************************************************************** euirom_vInit ***
 * Name:        Initialisation
 * Parameters:  None
 * Returns:     None
 * Description: Initialises Driver
 **************************************************************************************************/
extern void euirom_vInit(void);

/******************************************************************************* euirom_vI2cRead ***
 * Name:        I2C Read
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the EUIROM
 **************************************************************************************************/
extern void euirom_vI2cRead(uint8_t address, uint8_t *data, uint8_t nOfBytes);

/****************************************************************************** euirom_vI2cWrite ***
 * Name:        I2C Write
 * Parameters:  address
 *              data
 *              nOfBytes
 * Returns:     None
 * Description: Writes data to the EUIROM
 **************************************************************************************************/
extern void euirom_vI2cWrite(uint8_t address, uint8_t *data, uint8_t nOfBytes);

#endif /* EUIROM_H */
