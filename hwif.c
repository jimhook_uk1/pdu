/***************************************************************************************************
 * Name:    Hardware Interface
 * File:    hwif.c
 * Author:  RDTek
 * Date:    13/07/18
 *
 * Copyright:
 *
 *   Date   | Auth  | Comment
 * =========|=======================================================================================
 * 13/07/18 | RDTek | Created.
 *
 * Description:
 * System Clock is 25MHz. Peripheral Clock is 6.25MHz
 **************************************************************************************************/

/**************************************************************************************** Include */
#include "acdetect.h"
#include "display.h"
#include "eth.h"
#include "euirom.h"
#include "hdc1020.h"
#include "hwif.h"
#include "i2c.h"
#include "led.h"
#include "relays.h"
#include "switch.h"

/******************************************************************************* Global Variables */
int16_t hwif_s16Temperature;
uint16_t hwif_u16Humidity;
uint8_t hwif_u8Hz;

/*********************************************************************** Local Macros and Defines */
/* Pin definitions */
#define PIN_RESET       PORTEbits.RE0

/* EUI ROM */
#define EUIROM_EUI_ADD  0xFA
#define EUIROM_EUI_SIZE 6

//#define u8GetHighByte(x)    (uint8_t)((x) >> 8)
//#define u8GetLowByte(x)     (uint8_t)((x))
//#define u8GetUpperByte(x)   (uint8_t)((x) >> 16)
/********************************************************************************* Local TypeDefs */
enum etI2cState {
    I2C_STATE_CONV_TEMP,
    I2C_STATE_INACTIVE,
    I2C_STATE_IDLE,
    I2C_STATE_INIT_DISP,
    I2C_STATE_INIT_HDC,
    I2C_STATE_INIT_LED,
    I2C_STATE_INIT_RELAYS,
    I2C_STATE_RD_MAC,
    I2C_STATE_RD_TEMP,
    I2C_STATE_RELAYS_INIT_DONE,
    I2C_STATE_RELAYS_WR_DONE,
    I2C_STATE_WR_RELAYS,
    I2C_STATE_WR_DISP,
    I2C_STATE_WR_LEDS
};

/*********************************************************************** Local Constants (static) */

/*********************************************************************** Local Variables (static) */
static enum hwif_etEvent eEvent;
static uint16_t u16ChnARelay;
static uint16_t u16ChnBRelay;
static uint16_t u16Leds;
static uint8_t u8aMacAddress[6];
static uint8_t u8Event;
static uint8_t u8I2cErr;
static uint8_t u8I2cState;

/********************************************************************** Local Function Prototypes */
static void vFastTick(void);
static void vSlowTick(void);

/***************************************************************************** Callback Functions */

/******************************************************************************** i2c_vTxferDone ***
 * Name:        Transfer Done
 * Parameters:  err
 * Returns:     None
 * Description: Call back to indicate that the last transfer has been completed
 **************************************************************************************************/
void i2c_vTxferDone(uint8_t err) {
    switch (u8I2cState) {
        case I2C_STATE_RD_MAC:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_EUI_ROM_FAULT;
                u8aMacAddress[0] = 0x00;
                u8aMacAddress[1] = 0x04;
                u8aMacAddress[2] = 0xA3;
                u8aMacAddress[3] = 0x00;
                u8aMacAddress[4] = 0x00;
                u8aMacAddress[5] = 0x01;
            }
            (void)relays_u8I2cWrite();
            u8I2cState = I2C_STATE_INIT_RELAYS;
            break;
        case I2C_STATE_INIT_RELAYS:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_RELAY_FAULT;
            }
            if (relays_u8I2cWrite()) {
                u8I2cState = I2C_STATE_RELAYS_INIT_DONE;
            };
            break;
        case I2C_STATE_RELAYS_INIT_DONE:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_RELAY_FAULT;
            }
            led_vDeviceInit();
            u8I2cState = I2C_STATE_INIT_LED;
            break;
        case I2C_STATE_INIT_LED:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_TLC59116_FAULT;
            }
            hdc1020_vDeviceInit();
            u8I2cState = I2C_STATE_INIT_HDC;
            break;
        case I2C_STATE_INIT_HDC:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_HDC1020_FAULT;
            }
            (void)display_u8I2cWrite();
            u8I2cState = I2C_STATE_INIT_DISP;
            break;
        case I2C_STATE_INIT_DISP:
            if (err == I2C_ERR_NO_ACK) {
                u8I2cErr |= HWIF_I2C_DISPLAY_FAULT;
            }
            if (display_u8I2cWrite()) {
                u8I2cState = I2C_STATE_IDLE;
                eEvent = HWIF_EVT_I2C_INIT_DONE;
            }
            break;
        case I2C_STATE_CONV_TEMP:
        case I2C_STATE_RD_TEMP:
            (void)relays_u8I2cWrite();
            u8I2cState = I2C_STATE_WR_RELAYS;
            break;
        case I2C_STATE_WR_RELAYS:
            if (relays_u8I2cWrite()) {
                u8I2cState = I2C_STATE_RELAYS_WR_DONE;
            }
            break;
        case I2C_STATE_RELAYS_WR_DONE:
            led_vWriteLeds();
            u8I2cState = I2C_STATE_WR_LEDS;
            break;
        case I2C_STATE_WR_LEDS:
            if (display_u8I2cWrite()) {
                eEvent = HWIF_EVT_DISPLAY_WRITE_DONE;
            }
            u8I2cState = I2C_STATE_IDLE;
            break;
        case I2C_STATE_IDLE:
            if (eEvent != HWIF_EVT_NO_EVENT) {
                hwif_vI2CDeviceEvent(eEvent, u8I2cErr);
                eEvent = HWIF_EVT_NO_EVENT;
            }
            break;
    }
}

/******************************************************************************* Global Functions */

/************************************************************************************ hwif_vInit ***
 * Name:        Hardware Initialisation
 * Parameters:  None
 * Returns:     err (HWIF_I2C_FAULT)
 * Description: Initialises the microcontroller hardware
 **************************************************************************************************/
uint8_t hwif_u8Init(void) {
    uint8_t err;
    
    err = 0;

    /* Digital outputs */
    LATA = 0x00;
    TRISA = 0xEF;
    LATB = 0x00;
    TRISB = 0x07;
    LATC = 0x00;
    TRISC = 0x18;
    LATD = 0x00;
    TRISD = 0x20;
    LATE = 0x07;   /* RESET=1,CS1=1,CS2=1 */
    TRISE = 0x00;   /* All ouputs */
    TRISF = 0xFF;
    LATG = 0x00;
    TRISG = 0x0F;
    TRISH = 0xFF;
    LATJ = 0x00;
    TRISJ = 0x00;

    display_vInit();
    euirom_vInit();
    relays_vInit();
    rms_vInit();
    
    /* Initialise Ethernet */
    (void)eth_u8Init(u8aMacAddress);
    
    /* Initialise Timer3 and ECCP2 to automatically perform ADC conversion every 200usecs and
     * generate a high priority interrupt on completion */
    TMR3H = 0;
    TMR3L = 0;
    T3CON = 0x09;
    CCPR2H = 0x04;
    CCPR2L = 0xE2;
    CCP2CON = 0x0B;
    PIR1bits.ADIF = 0;
    PIE1bits.ADIE = 1;
    IPR1bits.ADIP = 1;
    
    /* Initialise Timer1 and ECCP1 to genrate a low priority interrupt every 10msecs */
    TMR1H = 0;
    TMR1L = 0;
    T1CON = 0x01;
    CCPR1H = 0xF4;
    CCPR1L = 0x24;
    CCP1CON = 0x0B;
    PIR1bits.CCP1IF = 0;
    PIE1bits.CCP1IE = 1;
    IPR1bits.CCP1IP = 0;
    
    /* Initialise interrupts */
    RCONbits.IPEN = 1;
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;

    /* I2C interface */
    if (i2c_u8Init() != I2C_ERR_IDLE) {
        err |= HWIF_I2C_FAULT;
    } else {
        eEvent = HWIF_EVT_NO_EVENT;
        euirom_vI2cRead(EUIROM_EUI_ADD, u8aMacAddress, EUIROM_EUI_SIZE);
        u8I2cErr = 0;
        u8I2cState = I2C_STATE_RD_MAC;
    }
    
    return err;
}

/************************************************************************************ hwif_vMain ***
 * Name:        Initialise Hardware Interface
 * Parameters:  None
 * Returns:     None
 * Description: Initialises the hardware interface handler
 **************************************************************************************************/
void hwif_vMain(void) {
    eth_vMain();
}
    
/********************************************************************************* hwif_vSetLeds ***
 * Name:        Set LED Outputs
 * Parameters:  leds
 * Returns:     None
 * Description: Sets the required led state to be written on next device transfer
 **************************************************************************************************/
void hwif_vSetLeds(uint16_t leds) {
    u16Leds = leds;
}

/******************************************************************************* hwif_vSetRelays ***
 * Name:        Set Relay Outputs
 * Parameters:  relays
 * Returns:     None
 * Description: Sets the relay outputs to the given demands
 **************************************************************************************************/
void hwif_vSetRelays(uint16_t relaysA, uint16_t relaysB) {
    u16ChnARelay = relaysA;
    u16ChnBRelay = relaysB;
}

/******************************************************************************** Local Functions */

/************************************************************************************* vFastTick ***
 * Name:        500usecs Timer Event
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
static void vFastTick(void) {
    acdetect_vDetect();
    rms_vLog();
}

/******************************************************************************* vHighPriorityIsr **
 * Name:        High Priority ISR
 * Parameters:  None
 * Returns:     None
 * Description: Handles the High Priority Interrupt which handles the 500usec tick.
 *              Checks for a voltage on the input and output of each channel by looking for both a
 *              0 and a 1 occuring within a 25msec window.
 *              Performs a free running log of the voltage input/current outputs and a log
 *              of the output voltage signal for use in post processing
 **************************************************************************************************/
static interrupt high_priority void vHighPriorityIsr(void){
    if (PIR1bits.ADIF == 1) {
        PIR1bits.ADIF = 0;
        vFastTick();
    } else if (PIR1bits.SSP1IF == 1) {
        i2c_vIsr();
    }
}

/******************************************************************************** vLowPriorityIsr **
 * Name:        Low Priority ISR
 * Parameters:  None
 * Returns:     None
 * Description: Handles the Low Priority Interrupt which handles the I2C interface and 10msec tick
 **************************************************************************************************/
static interrupt low_priority void vLowPriorityIsr(void){
    if (PIR1bits.CCP1IF == 1) {
        PIR1bits.CCP1IF = 0;
        vSlowTick();
    }
}

/************************************************************************************* vSlowTick ***
 * Name:        Ten msec Timer Event
 * Parameters:  None
 * Returns:     None
 * Description: 
 **************************************************************************************************/
static void vSlowTick(void) {
    static uint8_t state;
    static uint8_t tick;

    switch (state) {
        case 0:
           if (u8I2cState == I2C_STATE_IDLE) {
               state = 1;
           }
           break;
            
        case 1:
            if (u8I2cState == I2C_STATE_IDLE) {
                /* Get/Set I2C variables before starting transfers */
                hwif_s16Temperature = hdc1020_s16GetTemperature();
                hwif_u16Humidity = hdc1020_u16GetHumidity();
                hwif_u8Hz = (uint8_t)acdetect_u16GetHz();
                relays_vSetRelays(u16ChnARelay, u16ChnBRelay);
                led_vSetLeds(u16Leds);
                
                /* Start I2c transfers */
                if (tick == 0) {
                    hdc1020_vStartConversion();
                    u8I2cState = I2C_STATE_CONV_TEMP;
                } else {
                    hdc1020_vGetResults();
                    u8I2cState = I2C_STATE_RD_TEMP;
                }
                tick ++;
                if (tick == 100) {
                    tick = 0;
                }
            }
            rms_vMain();
            
            break;
    }
    
    switch_vMain();
    
    /* Let the application do something */
    hwif_vTick();
}
